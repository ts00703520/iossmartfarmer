//
//  BarChartViewController.swift
//  argiculture
//
//  Created by Thanachai on 5/17/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import UIKit
//import Charts

class BarChartViewController: UIViewController {
    
    
//    @IBOutlet weak var lineChartView: LineChartView!
//    @IBOutlet weak var barChartView: BarChartView!
    var months: [String]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        let myTitle = NSAttributedString(string: "You need to provide data for the chart.", attributes: [NSFontAttributeName:UIFont(name: "DTAC 2017 Regular", size: 10.0)!,NSForegroundColorAttributeName:UIColor.black])
//        barChartView.
//        lineChartView.delegate = self
//        barChartView.noDataText = "You need to provide data for the chart."
//        barChartView.noDataTextDescription = "GIVE REASON"
        
        let months = ["Jan" , "Feb", "Mar", "Apr", "May", "June", "July", "August", "Sept", "Oct", "Nov", "Dec"]
        
        let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]

        setChart(dataPoints: months, values: unitsSold)
       
//
//        var dataEntries: [BarChartDataEntry] = []
//        
//        for i in 0..<months.count {
//            let dataEntry = BarChartDataEntry(value: unitsSold[i], xIndex: i)
//            dataEntries.append(dataEntry)
//        }
//        
//        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
//        let chartData = BarChartData(xVals: months, dataSet: chartDataSet)
//        barChartView.data = chartData
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func setChart(dataPoints: [String], values: [Double]) {
        
//        var dataEntries: [ChartDataEntry] = []
//        
//        for i in 0..<dataPoints.count {
//            let dataEntry = ChartDataEntry(x: values[i], y: Double(i))
//            dataEntries.append(dataEntry)
//        }
//        
//        let chartDataSet = LineChartDataSet(values: dataEntries, label: "Units Sold")
//        chartDataSet.axisDependency = .left // Line will correlate with left axis values
//        chartDataSet.setColor(UIColor.red.withAlphaComponent(0.5)) // our line's opacity is 50%
//        chartDataSet.setCircleColor(UIColor.red) // our circle will be dark red
//        chartDataSet.lineWidth = 2.0
//        chartDataSet.circleRadius = 6.0 // the radius of the node circle
//        chartDataSet.fillAlpha = 65 / 255.0
//        chartDataSet.fillColor = UIColor.red
//        chartDataSet.highlightColor = UIColor.white
//        chartDataSet.drawCircleHoleEnabled = true
//        let chartData = LineChartData(dataSet: chartDataSet)
//        lineChartView.data = chartData
    }
}
