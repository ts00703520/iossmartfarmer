//
//  EditPlantTableViewCell.swift
//  argiculture
//
//  Created by thanachais on 18/4/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class EditPlantTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPlant: UIImageView!
    
    @IBOutlet weak var lblPlant: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
