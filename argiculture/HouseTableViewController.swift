//
//  HouseTableViewController.swift
//  argiculture
//
//  Created by thanachais on 3/4/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class HouseTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
   
    @IBOutlet weak var tblHouse: UITableView!
    
    let expandCellRowSize = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblHouse.dataSource = self
        self.tblHouse.delegate = self
        self.tblHouse.backgroundColor = UIColor.clear
        self.tblHouse.isScrollEnabled = false
        
        self.tblHouse.separatorColor = UIColor.clear
        
    }
    
    public func reloadData() {
        
//        print("self.childViewControllers \(self.childViewControllers)")
//        var tv = self.childViewControllers[0]
//        tv.mytableview.reloadData()
//        tv.viewWillAppear(true)
        self.tblHouse.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Shared.HOUSE_ROW_NO = 1
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Shared.HOUSE_ROW_NO + expandCellRowSize
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        if ( Shared.HOUSE_ROW_NO > 0 ) {
            
            if ( indexPath.row != Shared.HOUSE_ROW_NO ) {
                let cellTmp = tableView.dequeueReusableCell(withIdentifier: "HouseTableViewCell", for: indexPath) as! HouseTableViewCell
                
                cellTmp.backgroundColor = UIColor.white
                cellTmp.lblHouseName.text = "KUijung"
                
                cell = cellTmp
                
            } else {
                let cellTmp = tableView.dequeueReusableCell(withIdentifier: "HouseExpandTableViewCell", for: indexPath) as! HouseExpandTableViewCell
                cellTmp.backgroundColor = UIColor.clear
                
                cellTmp.imgArrow.target(forAction: #selector(self.showSelectHouse), withSender: self)
                
//                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.showSelectHouse(_:)))
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self.parent, action: #selector(NewPlantViewControllerV2.showSelectHouse(_:)))
                tapGestureRecognizer.numberOfTapsRequired = 1
                
                cellTmp.imgArrow.isUserInteractionEnabled = true
                
                cellTmp.imgArrow.addGestureRecognizer(tapGestureRecognizer)
                
                cell = cellTmp
                
            }
        }
        
        cell.selectionStyle = .none
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ( indexPath.row != Shared.HOUSE_ROW_NO ) {
            return Shared.HOUSE_HEIGHT
        } else {
            return Shared.HOUSE_ARROW_HEIGHT
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func showSelectHouse(_ sender: UITapGestureRecognizer) {
        print("arrow clickxxxx ")
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
