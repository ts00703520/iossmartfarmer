//
//  ImageCollectionCell.swift
//  argiculture
//
//  Created by thanachais on 14/3/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblInfo: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
}
