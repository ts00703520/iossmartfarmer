//
//  HouseExpandTableViewCell.swift
//  argiculture
//
//  Created by thanachais on 3/4/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class HouseExpandTableViewCell: UITableViewCell {

    @IBOutlet weak var imgArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
