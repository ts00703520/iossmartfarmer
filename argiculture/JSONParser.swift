//
//  JSONParser.swift
//  argiculture
//
//  Created by Thanachai on 3/8/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import Foundation
import UIKit

class JSONParser {
    
        
    static func parse(function: Function, data: Data) -> AnyObject? {
        
        var retObject: AnyObject?
        
        switch function {
        case Function.authen:
            do {
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                let token = Util.str( json["token"] as? String )
                
                let user = json["user"] as? [String: AnyObject]
                let name = Util.str( user?["name"] as? String )
                
                let authen = AUTHEN.init(errorCode: errorCode, errorMessage: errorMessage, token: token, user: (name))
                
                retObject = authen as AnyObject
                
                
            } catch {
                
            }
            
            break
        
        case Function.isValidToken:
            do {
                print("is valid token function -> data: \(NSString(data: data, encoding: String.Encoding.utf8.rawValue))")
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                print("json:", json)
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                
                let isValidToken = IS_VALID_TOKEN.init(errorCode: errorCode, errorMessage: errorMessage)
                
                retObject = isValidToken as AnyObject
                
                
            } catch {
                
            }
            
            break
        case Function.listPlant:
            do {
                print("list plant function -> data: \(NSString(data: data, encoding: String.Encoding.utf8.rawValue))")
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                print("json:", json)
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                let plant = Util.str( json["plant"] as? String )
                let rate = Util.str( json["rate"] as? String )
                
                var type = "", temp_min = "", temp_max = "", hu_min = "", hu_max = "", light_min = "", light_max = "", soil_hu_min = "", soil_hu_max = "", co2_min = "", co2_max = ""
                
                
                if ( json["range"] != nil && json["range"]?.count ?? 0 > 0 ) {
                    let range = json["range"] as! [String: AnyObject]
                    type = Util.str( range["range"] as? String )
                    temp_min = Util.str( range["temp_min"] as? String )
                    temp_max = Util.str( range["temp_max"] as? String )
                    hu_min = Util.str( range["hu_min"] as? String )
                    hu_max = Util.str( range["hu_max"] as? String )
                    light_min = Util.str( range["light_min"] as? String )
                    light_max = Util.str( range["light_max"] as? String )
                    soil_hu_min = Util.str( range["soi_hu_min"] as? String )
                    soil_hu_max = Util.str( range["soi_hu_max"] as? String )
                    co2_min = Util.str( range["co2_min"] as? String )
                    co2_max = Util.str( range["co2_max"] as? String )
                }
                
                
                let list = json["list"] as AnyObject
                
                
                
                let listPlant = LIST_PLANTS(errorCode: errorCode, errorMessage: errorMessage, plant: plant, rate: rate, range: (type: type, temp_min: temp_min, temp_max: temp_max, hu_min: hu_min, hu_max: hu_max, light_min: light_min, light_max: light_max, soi_hu_min: soil_hu_min, soi_hu_max: soil_hu_max, co2_min: co2_min, co2_max: co2_max), list: list)
                
                retObject = listPlant as AnyObject
                Shared.plantArray = list as? NSArray
                
            } catch {
                
            }
            
            break
        
        case Function.logout:
            
            do {
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                
                let logout = LOGOUT.init(errorCode: errorCode, errorMessage: errorMessage)
                
                retObject = logout as AnyObject
                
                
            } catch {
                
            }
            
            break
            
        case Function.getData:
            
            do {
                print("get data function -> data: \(NSString(data: data, encoding: String.Encoding.utf8.rawValue))")
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                
                print("get data json:", json)
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                var listImage = [IMAGE]()
                if ( json["image"] != nil ) {
                    let images = json["image"] as! NSArray
                    for image in images {
                        let getImage = image as! NSDictionary
                        let tmp = IMAGE.init(image_url: getImage.value(forKey: "image_url") as! String)
                        listImage.append(tmp)
                    }
                    
                }
                
                var listGetDataSet = [GET_DATA_SET]()
                if ( json["data"] != nil ) {
                    
                    let listData = json["data"] as! NSArray
                    for item in listData {
                        let getDataSet = item as! NSDictionary
                        
                        let tmpDataDetail = getDataSet.value(forKey: "data") as! NSDictionary
                        let dataDetail = DATA.init(
                            datetime: tmpDataDetail.value(forKey: "datetime") as! String
                            , data1: tmpDataDetail.value(forKey: "data1") as! String
                            , data2: tmpDataDetail.value(forKey: "data2") as! String
                            , data3: tmpDataDetail.value(forKey: "data3") as? String
                            , data4: tmpDataDetail.value(forKey: "data4") as? String
                            , data5: tmpDataDetail.value(forKey: "data5") as? String
                            , data6: tmpDataDetail.value(forKey: "data6") as? String
                            , data7: tmpDataDetail.value(forKey: "data7") as? String
                            , data8: tmpDataDetail.value(forKey: "data8") as? String
                            , type1: tmpDataDetail.value(forKey: "type1") as! String
                            , type2: tmpDataDetail.value(forKey: "type2") as! String
                            , type3: tmpDataDetail.value(forKey: "type3") as? String
                            , type4: tmpDataDetail.value(forKey: "type4") as? String
                            , type5: tmpDataDetail.value(forKey: "type5") as? String
                            , type6: tmpDataDetail.value(forKey: "type6") as? String
                            , type7: tmpDataDetail.value(forKey: "type7") as? String
                            , type8: tmpDataDetail.value(forKey: "type8") as? String
                            , unit1: tmpDataDetail.value(forKey: "unit1") as! String
                            , unit2: tmpDataDetail.value(forKey: "unit2") as! String
                            , unit3: tmpDataDetail.value(forKey: "unit3") as? String
                            , unit4: tmpDataDetail.value(forKey: "unit4") as? String
                            , unit5: tmpDataDetail.value(forKey: "unit5") as? String
                            , unit6: tmpDataDetail.value(forKey: "unit6") as? String
                            , unit7: tmpDataDetail.value(forKey: "unit7") as? String
                            , unit8: tmpDataDetail.value(forKey: "unit8") as? String
                            
                        )
                        let listDataIo = NSMutableArray()
                        if ( tmpDataDetail.value(forKey: "io") != nil) {
                            let listIo = tmpDataDetail.value(forKey: "io") as! [NSDictionary]
                            for io in listIo {
                                
                                let dataIo = IO.init(channel: io.value(forKey: "channel") as! String ,
                                        status: io.value(forKey: "status") as! String,
                                        sn: io.value(forKey: "sn") as! String,
                                        type: io.value(forKey: "type") as! String)
                                listDataIo.add(dataIo)
                            }
                            
                        }
                        var range: RANGE? = nil
                        if ( getDataSet.value(forKey: "plant") != nil ) {
                            let tmpPlant = getDataSet.value(forKey: "plant") as! NSDictionary
                            
                            range = RANGE.init(type: tmpPlant.value(forKey: "name") as! String,
                                               temp_min: tmpPlant.value(forKey: "temp_min") as! String,
                                               temp_max: tmpPlant.value(forKey: "temp_max") as! String,
                                               hu_min: tmpPlant.value(forKey: "hu_min") as! String,
                                               hu_max: tmpPlant.value(forKey: "hu_max") as! String,
                                               light_min: tmpPlant.value(forKey: "light_min") as! String,
                                               light_max: tmpPlant.value(forKey: "light_max") as! String,
                                               soi_hu_min: tmpPlant.value(forKey: "soi_hu_min") as! String,
                                               soi_hu_max: tmpPlant.value(forKey: "soi_hu_max") as! String,
                                               co2_min: tmpPlant.value(forKey: "co2_min") as? String,
                                               co2_max: tmpPlant.value(forKey: "co2_max") as? String)
                            
                        }
                        
                        let tmpData = GET_DATA_SET.init(sn: getDataSet.value(forKey: "sn") as! String,
                                                        dispName: getDataSet.value(forKey: "disp_name") as! String,
                                                        batt: getDataSet.value(forKey: "batt") as! String,
                                                        btn: getDataSet.value(forKey: "btn") as! String,
                                                        sgn: getDataSet.value(forKey: "sgn") as! String,
                                                        key: getDataSet.value(forKey: "key") as? String ?? nil,
                                                        secret: getDataSet.value(forKey: "secret") as? String ?? nil,
                                                        plant: range,
                                                        data: dataDetail,
                                                        io: listDataIo as? [IO]
                                                        
                            )
                        
                        
                        listGetDataSet.append(tmpData)
                        //                    print("getDataSet:", getDataSet)
                    }

                    
                }
                
                
                
                
                let getData = GET_DATA.init(errorCode: errorCode, errorMessage: errorMessage, data: listGetDataSet, image: listImage)
                
                retObject = getData as AnyObject
                
                
            } catch {
                
            }

            
            
            break
        case Function.settings:
            
            do {
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                let enableNoti = Util.str( json["enablenoti"] as? String )
                let startTime = Util.str( json["starttime"] as? String )
                let endTime = Util.str( json["endtime"] as? String )
//                let fcm = Util.str( json["fcm"] as? String )
                
                
                let saveSetting = SETTINGS.init(errorCode: errorCode, errorMessage: errorMessage, enablenoti: enableNoti, starttime: startTime, endtime: endTime)
                
                retObject = saveSetting as AnyObject
                
                
            } catch {
                
            }
            
            break
//        default:
//            
//            break
            
        case Function.refreshFcm:
            
            do {
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
              
                
                
                let refreshFcm = REFRESH_FCM.init(errorCode: errorCode, errorMessage: errorMessage)
                
                retObject = refreshFcm as AnyObject
                
                
            } catch {
                
            }
            
            break
            
        case Function.listHouse:
            do {
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                
                var listHouse = [HOUSE]()
                if ( json["house"] != nil ) {
                    let houses = json["house"] as! NSArray
                    for house in houses {
                        let getHouse = house as! NSDictionary
                        let tmp = HOUSE.init(name: getHouse.value(forKey: "name") as! String,
                                             id: getHouse.value(forKey: "id") as! String)
                        listHouse.append(tmp)
                    }
                    
                }
                
                let retlistHouse = LIST_HOUSE.init(errorCode: errorCode, errorMessage: errorMessage, house: listHouse)
                
                retObject = retlistHouse as AnyObject
                
                
            } catch {
                print(error)
            }
            break
            
        case Function.setPlant:
            do {
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                let changed = Util.str( json["changed"] as? String )
                
                let retSetPlant = SET_PLANT.init(errorCode: errorCode, errorMessage: errorMessage, changed: changed)
                
                retObject = retSetPlant as AnyObject
                
                
            } catch {
                print(error)
            }
            break
            
        case Function.version:
            do {
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                let version = Util.str( json["version"] as? String )
                
                let retVersion = VERSION.init(errorCode: errorCode, errorMessage: errorMessage, version: version)
                
                retObject = retVersion as AnyObject
                
                
            } catch {
                print(error)
            }
            break
        
        case Function.control:
            do {
                let string1 = String(data: data, encoding: String.Encoding.utf8) ?? "Data could not be printed"
                print(string1)
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                
                var data: CONTROL_RESP?
                if ( json["data"] != nil ) {
                    let getData = json["data"] as! NSDictionary
                    
                    data = CONTROL_RESP.init(code: getData.value(forKey: "code") as? String,
                                                message: getData.value(forKey: "message") as? String,
                                                httpCode: getData.value(forKey: "http_code") as? String
                                                )
                    
                }
                
                let retControl = CONTROL.init(errorCode: errorCode, errorMessage: errorMessage, data: data)
                
                retObject = retControl as AnyObject
                
                
            } catch {
                print(error)
            }
            break
            
        case Function.plantSetting:
            do {
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    else { break  }
                
                let errorCode = Util.str( json["error_code"] as? String )
                let errorMessage = Util.str( json["error_message"] as? String )
                
                var listRange = [RANGE]()
                if ( json["list"] != nil && json["list"]?.count ?? 0 > 0 ) {
                    let lists = json["list"] as! NSArray
                    
                    for range in lists {
                        let getRange = range as! NSDictionary
                        let tmp = RANGE.init(type: getRange.value(forKey: "name") as! String,
                                             temp_min: getRange.value(forKey: "temp_min") as! String,
                                             temp_max: getRange.value(forKey: "temp_max") as! String,
                                             hu_min: getRange.value(forKey: "hu_min") as! String,
                                             hu_max: getRange.value(forKey: "hu_max") as! String,
                                             light_min: getRange.value(forKey: "light_min") as! String,
                                             light_max: getRange.value(forKey: "light_max") as! String,
                                             soi_hu_min: getRange.value(forKey: "soi_hu_min") as! String,
                                             soi_hu_max: getRange.value(forKey: "soi_hu_max") as! String,
                                             co2_min: getRange.value(forKey: "co2_min") as? String,
                                             co2_max: getRange.value(forKey: "co2_max") as? String)
                                             
                        listRange.append(tmp)
                    }
                    
                }
                
                let retPlantSetting = PLANT_SETTING.init(errorCode: errorCode, errorMessage: errorMessage, list: listRange)
                
                retObject = retPlantSetting as AnyObject
                
                
            } catch {
                print(error)
            }
            break
            
            //        default:
            //
            //            break
        }
        
        
        
        return retObject!
    }
}
