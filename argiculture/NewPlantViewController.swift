//
//  NewPlantViewController.swift
//  argiculture
//
//  Created by Thanachai on 3/28/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import Foundation
import UIKit
import SideMenu
import Firebase

class NewPlantViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var tblSensor: UITableView!
    
    @IBOutlet weak var butMenu: UIButton!
    @IBOutlet weak var butUser: UIButton!
    @IBOutlet weak var lblUser: UILabel!
    
    @IBOutlet weak var lblFullname: UILabel!
    
    @IBOutlet weak var lblUserId: UILabel!
    @IBOutlet weak var lblPlant: UILabel!
    
    @IBOutlet weak var pickerPlant: UIPickerView!
    
    @IBOutlet weak var lblDisplayPlant: UILabel!
    @IBOutlet weak var butEdit: UIButton!
    
    static var timer: Timer? = nil
    var listPlant: LIST_PLANTS!
    var hu_min = 0
    var hu_max = 0
    var soil_min = 0
    var soil_max = 0
    var light_min = 0
    var light_max = 0
    var temp_min = 0
    var temp_max = 0
    
    let cellSpacingHeight: CGFloat = 10
    
    var plantArray: NSArray = []
    var getData: GET_DATA!

    typealias FINISHED_REFRESH_PLANT = () -> ()
    
    
    // MARK: View Controller
    override func viewDidLoad() {
        
        tblSensor.delegate = self
        tblSensor.dataSource = self
        
        pickerPlant.delegate = self
        pickerPlant.dataSource = self
        
        let backgroundImage = UIImage(named: "background_table.png")
        let imageView = UIImageView(image: backgroundImage)
        imageView.contentMode = .scaleAspectFit
        
    
        self.tblSensor.backgroundView = imageView
        
        
        // Define the menus
        
        SideMenuManager.default.leftMenuNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
        
//        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
//        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        lblUser.text = SeedUtil.userLogin
        lblFullname.text = NSLocalizedString("USERNAME", comment: "") + SeedUtil.userLogin
        lblUserId.text = NSLocalizedString("USERNAME", comment: "") + SeedUtil.userLogin
        
        SeedUtil.checkToken(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken, completionHandler: { result, message in
            if (result == 0) {
                print("view did appear check token success")
                
                self.refreshPlant() { () -> () in
                    self.refreshGetData()
                }
                
                
            } else if (result > 1 || result < 0) {
                DispatchQueue.main.async {
                    
                    let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "ViewController")
                    self.show(vc as! UIViewController, sender: vc)
                }
            }
        })
        
        
        
        startTimer()
        
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        
//        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)

        // Similarly, to dismiss a menu programmatically, you would do this:
//        dismiss(animated: true, completion: nil)
        
        // For Swift 2.3 (no longer maintained), use:
        // presentViewController(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    /*
    override func viewWillAppear(_ animated: Bool) {
     //   self.tblSensor.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
        
        // Add a background view to the table view
        let backgroundImage = UIImage(named: "background.png")
        let imageView = UIImageView(image: backgroundImage)
        imageView.contentMode = .scaleAspectFit
        
        self.tblSensor.backgroundView = imageView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
    */
    
    //MARK: Delegates
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return plantArray[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        print("select plant is: \(row) \(self.plantArray[row])")
        
        SeedUtil.listPlant(token: SeedUtil.token, plant: self.plantArray[row] as! String, completionHandler: {
            
            result, message, obj in
            
            if ( result == 0 ) {
                
                //                let listPlant = obj as! LIST_PLANTS
                self.listPlant = obj as! LIST_PLANTS
                
                self.plantArray = self.listPlant.list as! NSArray
                let selectedItem = self.findSelectedPlant(plant: self.listPlant.plant)
                
                DispatchQueue.main.async {
                    self.pickerPlant.reloadAllComponents()
                    //                    if ( self.pickerPlant.numberOfComponents ) {
                    //                        self.pickerPlant.selectedRow(inComponent: selectedItem)
                    //                    }
                    self.pickerPlant.selectRow(selectedItem, inComponent: 0, animated: false)
                    self.lblDisplayPlant.text = self.listPlant.plant
                    
                }
                
                
                self.refreshGetData()
                
                
                print("list array value is \(self.plantArray)")
                
                
            } else {
                
                print("error listplan \(message)")
            }
            
            
        })
        
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as! UILabel?
        //        DispatchQueue.main.async {
        
        if view == nil {  //if no label there yet
            pickerLabel = UILabel()
            //color the label's background
            //            let hue = CGFloat(row)/CGFloat(self.plantArray.count)
            //            pickerLabel?.backgroundColor = UIColor(hue: hue, saturation: 1.0, brightness: 1.0, alpha: 1.0)
        }
        
        let titleData = self.plantArray[row]
        let myTitle = NSAttributedString(string: titleData as! String, attributes: [NSAttributedString.Key.font:UIFont(name: "DTAC 2017 Regular", size: 10.0)!,NSAttributedString.Key.foregroundColor:UIColor.black])
        pickerLabel!.attributedText = myTitle
        pickerLabel!.textAlignment = .center
        //            }
        return pickerLabel!
    }
    
    
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return plantArray.count
    }
    
    func findSelectedPlant(plant: String) -> Int {
        var count = 0
        
        if (self.plantArray.count > 0) {
            
            for i in 0 ..< self.plantArray.count  {
                if (plant == self.plantArray[i] as! String) {
                    break
                }
                count += 1
            }
            
            
        }
        
        
        return count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ( self.getData != nil ) {
            //            print("numberOfRowsInSection self.getData.data.count: ", self.getData.data.count)
            return self.getData.data.count
        } else {
            //            print("numberOfRowsInSection 0")
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.backgroundColor = UIColor(white: 1, alpha: 0.5)
    cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        print("cell for row index path \(indexPath)")
        
        var min = ""
        var max = ""
        var divideBy1000_1: Bool = false
        var divideBy1000_2: Bool = false
        var faceFlag1 = 0
        var faceFlag2 = 0
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sensorCell", for: indexPath) as! NewSensorTableViewCell
        
        DispatchQueue.main.async {
            
            /*
             cell.contentView.layer.cornerRadius = 10
             cell.contentView.layer.masksToBounds = true
             cell.clipsToBounds = true
             cell.layer.borderColor = UIColor.blue.cgColor
             //                    cell.layer.borderColor = UIColor(red: 21.0, green: 72.0, blue: 129.0, alpha: 0.5).cgColor
             cell.layer.borderWidth = 1
             */
            /*
            cell.contentView.backgroundColor = UIColor.clear
            
            let whiteRoundedView : UIView = UIView(frame: CGRect(x: 5, y: 5, width: cell.contentView.frame.size.width - 10, height: cell.contentView.frame.size.height - 10 ))
            
            //            whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [0.5, 0.5, 0.5, 0.5])
            whiteRoundedView.layer.backgroundColor = UIColor.white.cgColor
            whiteRoundedView.layer.masksToBounds = false
            whiteRoundedView.layer.cornerRadius = 2.0
            whiteRoundedView.layer.shadowOffset = CGSize(width: -1, height: 1)
            whiteRoundedView.layer.shadowOpacity = 0.2
            
            cell.contentView.addSubview(whiteRoundedView)
            cell.contentView.sendSubview(toBack: whiteRoundedView)
            */
            
            // ================== sensor no. ==================
            cell.lblSensorNo.text = String(format: "%02d", indexPath.row + 1)
            
            
            
            // ================== data1 ==================
            
            
            //cell.lblWarning1.isHidden = true
            
            
            print(" cell for row index path \(indexPath) type: \(self.getData.data[indexPath.row].data.type1)")
            
            switch self.getData.data[indexPath.row].data.type1 {
            case "ความชื้น":
                cell.imgType1.image = UIImage(named: "air_humidity")
                cell.lblSensor1.text = NSLocalizedString("AIR_HUMIDITY", comment: "")
                
                min = self.listPlant.range.hu_min
                max = self.listPlant.range.hu_max
                
            //    cell.lblWarning1.text = NSLocalizedString("AIR_HUMIDITY_1_HIGH", comment: "")
                /*
                 if (LOCALE == "th") {
                 cell.lblWarning1.text = "ความชื้นสูง1 โปรดตรวจสอบ!!"
                 } else {
                 cell.lblWarning1.text = "Air Humidity1 High, Check!!"
                 }
                 */
                
                //                if (Float(self.getData.data[indexPath.row].data.data1)! > Float(max)!) {
                //                    cell.lblWarning1.isHidden = false
                //                } else {
                //                    cell.lblWarning1.isHidden = true
                //                }
                
                
                break
            case "อุณหภูมิ":
                cell.imgType1.image = UIImage(named: "temperature")
                cell.lblSensor1.text = NSLocalizedString("TEMPERATURE", comment: "")
                
                min = self.listPlant.range.temp_min
                max = self.listPlant.range.temp_max
                
              //  cell.lblWarning1.text = NSLocalizedString("TEMPERATURE_1_HIGH", comment: "")
                /*
                 if (LOCALE == "th") {
                 cell.lblWarning1.text = "อุณหภูมิสูง1 โปรดตรวจสอบ!!"
                 } else {
                 cell.lblWarning1.text = "Temperature1 High, Check!!"
                 }
                 */
                
                
                break
            case "แสง":
                cell.imgType1.image = UIImage(named: "light")
                cell.lblSensor1.text = NSLocalizedString("LIGHT", comment: "")
                
                min = self.listPlant.range.light_min
                max = self.listPlant.range.light_max
                
            //    cell.lblWarning1.text = NSLocalizedString("LIGHT_1_HIGH", comment: "")
                /*
                 if (LOCALE == "th") {
                 cell.lblWarning1.text = "แสงสว่างมาก1 โปรดตรวจสอบ!!"
                 } else {
                 cell.lblWarning1.text = "Light1 High, Check!!"
                 }
                 */
                
//                divideBy1000_1 = true
                 divideBy1000_1 = false
                break
            default:
                cell.imgType1.image = UIImage(named: "soil_humidity")
                cell.lblSensor1.text = NSLocalizedString("SOIL_HUMIDITY", comment: "")
                
                min = self.listPlant.range.soi_hu_min
                max = self.listPlant.range.soi_hu_max
                
            //    cell.lblWarning1.text = NSLocalizedString("SOIL_HUMIDITY_1_HIGH", comment: "")
                /*
                 if (LOCALE == "th") {
                 cell.lblWarning1.text = "ความชื้นในดินสูง1 โปรดตรวจสอบ!!"
                 } else {
                 cell.lblWarning1.text = "Soil Humidity1 High, Check!!"
                 }
                 */
                
            }
            //cell.lblUnit1.text = self.getData.data[indexPath.row].data.unit1
            
            cell.lblMin1.text = min
            cell.lblMax1.text = max
            
            var tempValue_1 = Float(self.getData.data[indexPath.row].data.data1)!
            if ( divideBy1000_1 ) {
                tempValue_1 = tempValue_1 / 1000
            }
            cell.lblValue1.text = String(format: "%.2f", tempValue_1 ) + " " + self.getData.data[indexPath.row].data.unit1
            
            if (tempValue_1 < Float(min)!) {
                cell.imgCircle1.image = UIImage(named: "blue_circle")
                faceFlag1 = -1
              //  cell.lblValue1.textColor = UIColor.cyan
            } else if (tempValue_1 > Float(max)!) {
                cell.imgCircle1.image = UIImage(named: "red_circle")
                faceFlag1 = 1
              //  cell.lblData1.textColor = UIColor.red
                
            //    cell.lblWarning1.isHidden = false
                //                self.sendNotification(message: "")
                
            } else {
                cell.imgCircle1.image = UIImage(named: "green_circle")
                faceFlag1 = 0
                //cell.lblData1.textColor = UIColor.green
            }
            
            
            // ================== data2 ==================
            //            cell.lblData2.text = self.getData.data[indexPath.row].data.data2
            
            
            
            //cell.lblWarning2.isHidden = true
            switch self.getData.data[indexPath.row].data.type2 {
            case "ความชื้น":
                cell.imgType2.image = UIImage(named: "air_humidity")
                cell.lblSensor2.text = NSLocalizedString("AIR_HUMIDITY", comment: "")
                
                min = self.listPlant.range.hu_min
                max = self.listPlant.range.hu_max
                
               // cell.lblWarning2.text = NSLocalizedString("AIR_HUMIDITY_2_HIGH", comment: "")
                /*
                 if (LOCALE == "th") {
                 cell.lblWarning2.text = "ความชื้นสูง2 โปรดตรวจสอบ!!"
                 } else {
                 cell.lblWarning2.text = "Air Humidity2 High, Check!!"
                 }
                 */
                
                break
            case "อุณหภูมิ":
                cell.imgType2.image = UIImage(named: "temperature")
                cell.lblSensor2.text = NSLocalizedString("TEMPERATURE", comment: "")
                
                min = self.listPlant.range.temp_min
                max = self.listPlant.range.temp_max
                
             //   cell.lblWarning2.text = NSLocalizedString("TEMPERATURE_2_HIGH", comment: "")
                
                /*
                 if (LOCALE == "th") {
                 cell.lblWarning2.text = "อุณหภูมิสูง2 โปรดตรวจสอบ!!"
                 } else {
                 cell.lblWarning2.text = "Temperature2 High, Check!!"
                 }
                 */
                
                break
            case "แสง":
                cell.imgType2.image = UIImage(named: "light")
                cell.lblSensor2.text = NSLocalizedString("LIGHT", comment: "")
                
                min = self.listPlant.range.light_min
                max = self.listPlant.range.light_max
                
              //  cell.lblWarning2.text = NSLocalizedString("LIGHT_2_HIGH", comment: "")
                /*
                 if (LOCALE == "th") {
                 cell.lblWarning2.text = "แสงสว่างมาก2 โปรดตรวจสอบ!!"
                 } else {
                 cell.lblWarning2.text = "Light2 High, Check!!"
                 }
                 */
                
//                divideBy1000_2 = true
                divideBy1000_2 = false
                
                break
            default:
                cell.imgType2.image = UIImage(named: "soil_humidity")
                cell.lblSensor2.text = NSLocalizedString("SOIL_HUMIDITY", comment: "")
                
                min = self.listPlant.range.soi_hu_min
                max = self.listPlant.range.soi_hu_max
                
             //   cell.lblWarning2.text = NSLocalizedString("SOIL_HUMIDITY_2_HIGH", comment: "")
                /*
                 if (LOCALE == "th") {
                 cell.lblWarning2.text = "ความชื้นในดินสูง2 โปรดตรวจสอบ!!"
                 } else {
                 cell.lblWarning2.text = "Soil Humidity2 High, Check!!"
                 }
                 */
                
            }
           // cell.lblUnit2.text = self.getData.data[indexPath.row].data.unit2
            
            cell.lblMin2.text = min
            cell.lblMax2.text = max
            
            var tempValue_2 = Float(self.getData.data[indexPath.row].data.data2)!
            if ( divideBy1000_2 ) {
                tempValue_2 = tempValue_2 / 1000
            }
            cell.lblValue2.text = String(format: "%.2f", tempValue_2 ) + " " + self.getData.data[indexPath.row].data.unit2
            
            
            if (tempValue_2 < Float(min)!) {
                cell.imgCircle2.image = UIImage(named: "blue_circle")
                faceFlag2 = -1
               // cell.lblData2.textColor = UIColor.cyan
            } else if (tempValue_2 > Float(max)!) {
                cell.imgCircle2.image = UIImage(named: "red_circle")
                faceFlag2 = 1
               // cell.lblData2.textColor = UIColor.red
                
               // cell.lblWarning2.isHidden = false
                
            } else {
                cell.imgCircle2.image = UIImage(named: "green_circle")
                faceFlag2 = 0
                // cell.lblData2.textColor = UIColor.green
            }
            
            // ================== battery ==================
            var batt = 0
            print("batt: ", self.getData.data[indexPath.row].batt)
            batt = Int( self.getData.data[indexPath.row].batt )!
            if ( batt == 100  ) {
                cell.imgBattery.image = UIImage(named: "new_battery_100")
            } else if (batt >= 60  ){
                cell.imgBattery.image = UIImage(named: "new_battery_60")
            } else if (batt >= 30  ){
                cell.imgBattery.image = UIImage(named: "new_battery_30")
            } else if (batt >= 5  ){
                cell.imgBattery.image = UIImage(named: "new_battery_1")
            } else {
                cell.imgBattery.image = UIImage(named: "new_battery_0")
            }
            
            
            // ================== connectivity ==================
            if ( self.getData.data[indexPath.row].sgn == "1") {
                cell.imgWireless.image = UIImage(named: "wireless_on")
            } else {
                cell.imgWireless.image = UIImage(named: "wireless_off")
            }
            
            // ================== datetime ==================
            let str = self.getData.data[indexPath.row].data.datetime
            let startDate = str.index(str.startIndex, offsetBy: 0)
            let endDate = str.index(str.endIndex, offsetBy: -9)
            let rangeDate = startDate..<endDate
            
            let startTime = str.index(str.startIndex, offsetBy: 10)
            let endTime = str.index(str.endIndex, offsetBy: 0)
            let rangeTime = startTime..<endTime
            
            cell.lblDateTime1.text = NSLocalizedString("DATE", comment: "") + self.getData.data[indexPath.row].data.datetime.substring(with: rangeDate) + "\n" +
                NSLocalizedString("TIME", comment: "") + self.getData.data[indexPath.row].data.datetime.substring(with: rangeTime)
            
            cell.lblDateTime2.text = NSLocalizedString("DATE", comment: "") + self.getData.data[indexPath.row].data.datetime.substring(with: rangeDate) + "\n" +
                NSLocalizedString("TIME", comment: "") + self.getData.data[indexPath.row].data.datetime.substring(with: rangeTime)
            
            // ================== Face indicator ==================
            
            if ( faceFlag1 >= 1 || faceFlag2 >= 1) {
                cell.imgFace.image = UIImage(named: "red_face")
            } else if ( faceFlag1 == 0 || faceFlag2 == 0 ) {
                cell.imgFace.image = UIImage(named: "green_face")
            } else {
                cell.imgFace.image = UIImage(named: "blue_face")
            }
            
            
            
        }
        
        
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("did select row")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
 
    
    
    
    
    // ==========================================================================
    // =====================================
    // MARK: - USER DEFINED FUNCTION
    // =====================================
    // ==========================================================================
    
    
    @objc func update() {
        DispatchQueue.main.async {
            self.refreshPlant{ () -> () in
                self.refreshGetData()
            }
            
        }
    }
    
    
    func startTimer() {
        let intervalTimer = 60.0 * Float(SeedUtil.refreshInterval)!
        if ( NewPlantViewController.timer != nil ) {
            
            print ("==========timer not nil  clear before timer task\n\n")
            NewPlantViewController.timer?.invalidate()
            NewPlantViewController.timer = nil
        }
        print ("==============   start new timer task --> \(intervalTimer)\n\n")
        
        NewPlantViewController.timer = Timer.scheduledTimer(timeInterval: TimeInterval(intervalTimer), target: self, selector: #selector(NewPlantViewController.update), userInfo: nil, repeats: true)
    }
    
    static func stopTimer() {
        
        print("Stop NewPlantViewController timer Task")
        if ( timer != nil ) {
            timer?.invalidate()
        }
        
    }
    
    func refreshPlant(completed: @escaping FINISHED_REFRESH_PLANT) {
        
        print("refreshPlant")
        SeedUtil.listPlant(token: SeedUtil.token, plant: "", completionHandler: {
            
            result, message, obj in
            
            if ( result == 0 ) {
                
                self.listPlant = obj as! LIST_PLANTS
                
                //                var hu_min = Int(self.listPlant.range.hu_min)
                //                var hu_max = Int(self.listPlant.range.hu_max)
                //                var soil_min = Int(self.listPlant.range.soi_hu_min)
                //                var soil_max = Int(self.listPlant.range.soi_hu_max)
                //                var light_min = Int(self.listPlant.range.light_min)
                //                var light_max = Int(self.listPlant.range.light_max)
                //                var temp_min = Int(self.listPlant.range.temp_min)
                //                var temp_max = Int(self.listPlant.range.temp_min)
                
                self.plantArray = self.listPlant.list as! NSArray
                let selectedItem = self.findSelectedPlant(plant: self.listPlant.plant)
                
                DispatchQueue.main.async {
                    self.pickerPlant.reloadAllComponents()
                    //                    if ( self.pickerPlant.numberOfComponents ) {
                    //                        self.pickerPlant.selectedRow(inComponent: selectedItem)
                    //                    }
                    self.pickerPlant.selectRow(selectedItem, inComponent: 0, animated: false)
                    self.lblDisplayPlant.text = self.listPlant.plant
                }
                
                
                
                print("list array value is \(self.plantArray)")
                
                
            } else {
                
                print("error listplan \(message)")
            }
            completed()
            
        })
        
    }
    
    
    
    func refreshGetData() {
        
        print("refreshGetData")
        
        SeedUtil.getData(token: SeedUtil.token, completionHandler: {
            
            result, message, obj in
            
            if ( result == 0 ) {
                
                self.getData = obj as! GET_DATA
                
                
                
                // print("get data value is \(getData)")
                print("total sensors is \(self.getData.data.count)")
                DispatchQueue.main.async {
                    self.tblSensor.reloadData()
                    Util.animateMyViews(viewToHide: self.tblSensor, viewToShow: self.tblSensor)
                    
                }
                
                
                
                
                for i in 0 ..< self.getData.data.count  {
                    print(" batt \(self.getData.data[i].batt) btn: \(self.getData.data[i].btn)")
                    print("data1: \(self.getData.data[i].data.data1) type1: \(self.getData.data[i].data.type1) unit1: \(self.getData.data[i].data.unit1)")
                    print("data2: \(self.getData.data[i].data.data2) type2: \(self.getData.data[i].data.type2) unit2: \(self.getData.data[i].data.unit2)")
                    
                }
                
            } else {
                
                print("error getdata \(message)")
            }
            
            
        })
    }
    
    // ====================================================== //
    // ===================== UI TAB BAR ===================== //
    // ====================================================== //
    
    //    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    //        print("item title =  \(item.tag)")
    //    }
    
    deinit {
        NewPlantViewController.timer?.invalidate()
    }
    
    @IBAction func logout(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("LOGOUT_TITLE", comment: ""), message: NSLocalizedString("LOGOUT_MESSAGE", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: UIAlertAction.Style.default, handler: { action in
        
            SeedUtil.logout(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken,   completionHandler: {
                
                result, message in
                
                if ( result == 0 ) {
                    
                    NewPlantViewController.stopTimer()
                    
                    DispatchQueue.main.async {
                        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                        self.show(vc as! UIViewController, sender: vc)
                    }
                    
                } else {
                    
                    
                    DispatchQueue.main.async {
                        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                        self.show(vc as! UIViewController, sender: vc)
                    }
                    
                }
                
                
            })
        
        
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
 
}
