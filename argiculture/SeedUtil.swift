//
//  SeedUtil.swift
//  argiculture
//
//  Created by Thanachai on 3/9/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import Foundation
import UIKit

class SeedUtil {
    
    static var TIMEOUT: Double = 60
    
    static var token = ""
    static var userLogin = ""
    static var refreshInterval = "5"
    static var enableNotification = "1"
    static var tokenFcm = ""
    static var startTime = "6"
    static var stopTime = "18"

    
    static var defaultRefreshInterval = "5"
    static var defaultStartTime = "6"
    static var defaultStopTime = "18"
    
    static let urlIsValidToken = "https://magicporting.dtac.co.th:12011/seedbox/isValidToken/"
    static let urlListPlant = "https://magicporting.dtac.co.th:12011/seedbox/listPlant/"
    static let urlLogout = "https://magicporting.dtac.co.th:12011/seedbox/logout/"
    static let urlGetData = "https://magicporting.dtac.co.th:12011/seedbox/getData/index_kui.php" // Production
//    static let urlGetData = "https://magicporting.dtac.co.th:12011/seedbox/getData/index_kui_nectecio.php"
    static let urlSaveSettings = "https://magicporting.dtac.co.th:12011/seedbox/notify/"
    static let urlRefreshFcm = "https://magicporting.dtac.co.th:12011/seedbox/refreshFcm/"
    static let urlNotiHistory = "https://magicporting.dtac.co.th:12011/seedbox/notiHistory/"
    static let urlListHouse = "https://magicporting.dtac.co.th:12011/seedbox/listHouse/"
    static let urlSetPlant = "https://magicporting.dtac.co.th:12011/seedbox/device/serial.php"
    static let urlVersion = "https://magicporting.dtac.co.th:12011/seedbox/version/"
    static let urlControl = "https://magicporting.dtac.co.th:12011/seedbox/control/" // Production
//    static let urlControl = "https://magicporting.dtac.co.th:12011/seedbox/control/index_nectecio.php"
    
    static let urlPlantSetting = "https://magicporting.dtac.co.th:12011/seedbox/listPlant/setting.php"
    
    static func checkToken(token: String, fcm: String?, completionHandler:  @escaping (_ result: Int, _ resultMessage: String) -> Void) {
    
    
        if ( Util.str(token).count > 0) {
        
            // let url = "https://magicporting.dtac.co.th:12011/seedbox/isValidToken/"
            var data = "token=" + token
            
            if ( Util.str(fcm).count > 0 ) {
                data += "&fcm=" + fcm!
            }
            
                
                HTTPUtil.async(url: urlIsValidToken, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
                    
                    if let error = err {
                        print("error:", error)
                        
                        completionHandler(-1, error.localizedDescription)
                        
                        
                    }
                    
//                    do {
                        guard let data = data else { return }
                        
                        let isValidToken = JSONParser.parse(function: Function.isValidToken, data: data) as! IS_VALID_TOKEN
                        
                        if (isValidToken.errorCode == "0") {
                            
                            completionHandler(0, "")
                            
                            // redirect to next page
                            // print("succeed check token goto next page right now")
                            
                        } else {
                            
                            completionHandler(1, isValidToken.errorMessage)
                        }
                        
                        
                        
//                    } catch {
//                        print("error:", error)
//                        
//                        completionHandler(-1, error.localizedDescription)
//                        
//                    }
                    
                    
                })
                
            
            
        
        
        } else {
             completionHandler(1, "no token")
        }
    
    
    }
    
    
    static func listPlant(token: String, plant: String, completionHandler:  @escaping (_ result: Int, _ resultMessage: String, _ obj: AnyObject?) -> Void) {
        
        var strToken = "token="
        var strPlant = "plant="
        
        
        if ( Util.str(token).count > 0) {
            strToken += token
        }
        if ( Util.str(plant).count > 0) {
            strPlant += plant
        }
        
        let data = strToken + "&" + strPlant
            
        
            HTTPUtil.async(url: urlListPlant, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
                
                if let error = err {
                    print("error:", error)
                    
                    completionHandler(-1, error.localizedDescription, nil)
                    
                    
                }
                
//                do {
                    guard let data = data else { return }
                    
                    let listPlant = JSONParser.parse(function: Function.listPlant, data: data) as! LIST_PLANTS
                    
                    if (listPlant.errorCode == "0") {
                        
                        completionHandler(0, "", listPlant as AnyObject)
                        
                        // redirect to next page
                        // print("succeed check token goto next page right now")
                        
                    } else {
                        
                        completionHandler(1, listPlant.errorMessage, nil)
                    }
                    
                    
                    
//                } catch {
//                    print("error:", error)
//                    
//                    completionHandler(-1, error.localizedDescription, nil)
//                    
//                }
                
                
            })
            
            
            
            
            
        }
        
        
    static func logout(token: String, fcm: String?, completionHandler:  @escaping (_ result: Int, _ resultMessage: String) -> Void) {
        
        var strToken = "token="
        
        
        
        if ( Util.str(token).count > 0) {
            strToken += token
        }
        if ( Util.str(fcm).count > 0) {
            strToken += "&fcm=" + fcm!
        }
        
        let data = strToken
        
        
        HTTPUtil.async(url: urlLogout, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
            
            if let error = err {
                print("error:", error)
                
                completionHandler(-1, error.localizedDescription)
                
                
            }
            
            //                do {
            guard let data = data else { return }
            
            let logout = JSONParser.parse(function: Function.logout, data: data) as! LOGOUT
            
            if (logout.errorCode == "0") {
                
                completionHandler(0, "")
                
                // redirect to next page
                // print("succeed check token goto next page right now")
                
            } else {
                
                completionHandler(1, logout.errorMessage)
            }
            
            
            
            //                } catch {
            //                    print("error:", error)
            //
            //                    completionHandler(-1, error.localizedDescription, nil)
            //
            //                }
            
            
        })
        
        
        
        
        
    }
    
    static func getData(token: String, completionHandler:  @escaping (_ result: Int, _ resultMessage: String, _ obj: AnyObject?) -> Void) {
        
        var strToken = "token="
        
        
        
        if ( Util.str(token).count > 0) {
            strToken += token
        }
        
        let data = strToken
        
        
        HTTPUtil.async(url: urlGetData, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
            
            if let error = err {
                print("error:", error)
                
                completionHandler(-1, error.localizedDescription, nil)
                
                
            }
            
            //                do {
            guard let data = data else { return }
            
            let getData = JSONParser.parse(function: Function.getData, data: data) as! GET_DATA
            
            if (getData.errorCode == "0") {
                
                completionHandler(0, "", getData as AnyObject?)
                
                // redirect to next page
                // print("succeed check token goto next page right now")
                
            } else {
                
                completionHandler(1, getData.errorMessage, nil)
            }
            
            
            
            //                } catch {
            //                    print("error:", error)
            //
            //                    completionHandler(-1, error.localizedDescription, nil)
            //
            //                }
            
            
        })
        
        
        
        
        
    }
    
    static func saveSettings(token: String, enableNoti: String, startTime: String, endTime: String, fcm: String, completionHandler:  @escaping (_ result: Int, _ resultMessage: String, _ obj: AnyObject?) -> Void) {
        
        var strToken = "token="
        
        
        
        if ( Util.str(token).count > 0) {
            strToken += token
        }
        if ( Util.str(enableNoti).count > 0) {
            strToken += "&enablenoti=" + enableNoti
        }
        if ( Util.str(startTime).count > 0) {
            strToken += "&starttime=" + startTime
        }
        if ( Util.str(endTime).count > 0) {
            strToken += "&endtime=" + endTime
        }
        if ( Util.str(fcm).count > 0) {
            strToken += "&fcm=" + fcm
        }
        
        let data = strToken
        
        
        HTTPUtil.async(url: urlSaveSettings, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
            
            if let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {
                    print("Success")
                } else {
                    completionHandler(-1, "GENERAL_ERROR", nil)
                    return
                }
            }
            
            if let error = err {
                print("error:", error)
                
                completionHandler(-1, error.localizedDescription, nil)
                return
                
            }
            
            //                do {
            guard let data = data else { return }
            
            
            
            let setting = JSONParser.parse(function: Function.settings, data: data) as! SETTINGS
            
            if (setting.errorCode == "0") {
                
                completionHandler(0, "", setting as AnyObject?)
                
                // redirect to next page
                // print("succeed check token goto next page right now")
                
            } else {
                
                completionHandler(1, setting.errorMessage, nil)
            }
            
            
            
            //                } catch {
            //                    print("error:", error)
            //
            //                    completionHandler(-1, error.localizedDescription, nil)
            //
            //                }
            
            
        })
        
        
        
        
        
    }
    
    static func refreshFcm(token: String, fcm: String?, completionHandler:  @escaping (_ result: Int, _ resultMessage: String) -> Void) {
        
        
        if ( Util.str(token).count > 0) {
            
            
            var data = "token=" + token
            
            if ( (fcm?.count)! > 0 ) {
                data += "&fcm=" + fcm!
            }
            
            
            HTTPUtil.async(url: urlRefreshFcm, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
                
                if let error = err {
                    print("error:", error)
                    
                    completionHandler(-1, error.localizedDescription)
                    
                    
                }
                
                //                    do {
                guard let data = data else { return }
                
                let refreshFcm = JSONParser.parse(function: Function.refreshFcm, data: data) as! REFRESH_FCM
                
                if (refreshFcm.errorCode == "0") {
                    
                    completionHandler(0, "")
                    
                    // redirect to next page
                    // print("succeed check token goto next page right now")
                    
                } else {
                    
                    completionHandler(1, refreshFcm.errorMessage)
                }
                
                
                
                //                    } catch {
                //                        print("error:", error)
                //
                //                        completionHandler(-1, error.localizedDescription)
                //
                //                    }
                
                
            })
            
            
            
            
            
        }
        
        
    }
    
    /*
    static func notiHistory(token: String, completionHandler:  @escaping (_ result: Int, _ resultMessage: String, _ obj: AnyObject?) -> Void) {
        
        
        if ( Util.str(token).count > 0) {
            
            
            let data = "token=" + token
            
            
            HTTPUtil.async(url: urlNotiHistory, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
                
                if let error = err {
                    print("error:", error)
                    
                    completionHandler(-1, error.localizedDescription, nil)
                    
                    return
                }
                
                //                    do {
                guard let data = data else { return }
                
                let notiHistory = JSONParser.parse(function: Function.notiHistory, data: data) as! NOTI_DATA
                
                if (notiHistory.errorCode == "0") {
                    
                    completionHandler(0, "", notiHistory as AnyObject?)
                    
                    // redirect to next page
                    // print("succeed check token goto next page right now")
                    
                } else {
                    
                    completionHandler(1, notiHistory.errorMessage, nil)
                }
                
                
                
                //                    } catch {
                //                        print("error:", error)
                //
                //                        completionHandler(-1, error.localizedDescription)
                //
                //                    }
                
                
            })
            
            
            
            
            
        }
        
        
    }
     */
    
    static func findSelectedPlant(plantArray: NSArray, plant: String) -> Int {
        var count = 0
        
        if (plantArray.count > 0) {
            
            for i in 0 ..< plantArray.count  {
                if (plant == plantArray[i] as! String) {
                    break
                }
                count += 1
            }
            
            
        }
        
        
        return count
    }
    
    static func listHouse(token: String, completionHandler:  @escaping (_ result: Int, _ resultMessage: String, _ obj: AnyObject?) -> Void) {
        
        var strToken = "token="
        
        
        if ( Util.str(token).count > 0) {
            strToken += token
        }
        
        
        let data = strToken + "&"
        
        
        HTTPUtil.async(url: urlListHouse, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
            
            if let error = err {
                print("error:", error)
                
                completionHandler(-1, error.localizedDescription, nil)
                
                
            }
            
            //                do {
            guard let data = data else { return }
            
            let listHouse = JSONParser.parse(function: Function.listHouse, data: data) as! LIST_HOUSE
            
            if (listHouse.errorCode == "0") {
                
                completionHandler(0, "", listHouse as AnyObject)
                
                // redirect to next page
                // print("succeed check token goto next page right now")
                
            } else {
                
                completionHandler(1, listHouse.errorMessage, nil)
            }
            
            
            
            //                } catch {
            //                    print("error:", error)
            //
            //                    completionHandler(-1, error.localizedDescription, nil)
            //
            //                }
            
            
        })
        
        
        
        
        
    }
    
    static func setPlant(token: String, serial: String, plant: String, completionHandler:  @escaping (_ result: Int, _ resultMessage: String, _ obj: AnyObject?) -> Void) {
        
        var strToken = "token="
        var strPlant = "plant="
        var strSerial = "sn="
        
        if ( Util.str(token).count > 0) {
            strToken += token
        }
        if ( Util.str(plant).count > 0) {
            strPlant += plant
        }
        if ( Util.str(serial).count > 0) {
            strSerial += serial
        }
        
        let data = strToken + "&" + strPlant + "&" + strSerial
        
        
        HTTPUtil.async(url: urlSetPlant, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
            
            if let error = err {
                print("error:", error)
                
                completionHandler(-1, error.localizedDescription, nil)
                
                
            }
            
            //                do {
            guard let data = data else { return }
            
            let setPlant = JSONParser.parse(function: Function.setPlant, data: data) as! SET_PLANT
            
            if (setPlant.errorCode == "0") {
                
                completionHandler(0, "", setPlant as AnyObject)
                
                // redirect to next page
                // print("succeed check token goto next page right now")
                
            } else {
                
                completionHandler(1, setPlant.errorMessage, nil)
            }
            
          
            
        })
        
        
        
        
        
    }
    
    static func checkVersion(os: String, completionHandler:  @escaping (_ result: Int, _ resultMessage: String, _ obj: AnyObject?) -> Void) {
        
        var strOs = "os="
        
        
        if ( Util.str(os).count > 0) {
            strOs += os
        }
        
        
        let data = strOs
        
        
        HTTPUtil.async(url: urlVersion, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
            
            if let error = err {
                print("error:", error)
                
                completionHandler(-1, error.localizedDescription, nil)
                
                
            }
            
            //                do {
            guard let data = data else { return }
            
            let checkVersion = JSONParser.parse(function: Function.version, data: data) as! VERSION
            
            if (checkVersion.errorCode == "0") {
                
                completionHandler(0, "", checkVersion as AnyObject)
                
                // redirect to next page
                // print("succeed check token goto next page right now")
                
            } else {
                
                completionHandler(1, checkVersion.errorMessage, nil)
            }
            
            
            
        })
        
        
        
        
        
    }
    
    static func control(token: String, sn: String, channel: String, message: String, completionHandler:  @escaping (_ result: Int, _ resultMessage: String, _ obj: AnyObject?) -> Void) {
        
        var strToken = "token="
        var strSn = "sn="
        var strChannel = "channel="
        var strMessage = "message="
        
        if ( Util.str(token).count > 0) {
            strToken += token
        }
        if ( Util.str(sn).count > 0) {
            strSn += sn
        }
        if ( Util.str(channel).count > 0) {
            strChannel += channel
        }
        if ( Util.str(message).count > 0) {
            strMessage += message
        }
        
        
        
        let data = strToken + "&" + strSn + "&" + strChannel + "&" + strMessage
        
        
        HTTPUtil.async(url: urlControl, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
            
            if let error = err {
                print("error:", error)
                
                completionHandler(-1, error.localizedDescription, nil)
                
                
            }
            
            //                do {
            guard let data = data else { return }
            
            let control = JSONParser.parse(function: Function.control, data: data) as! CONTROL
            
            if (control.errorCode == "0") {
                
                completionHandler(0, "", control as AnyObject)
                
                // redirect to next page
                // print("succeed check token goto next page right now")
                
            } else {
                
                completionHandler(1, control.errorMessage, nil)
            }
            
            
            
        })
        
        
        
        
        
    }
    
    static func plantSetting(token: String, plant: String, new_name: String,
                        temp_min: String, temp_max: String,
                        hu_min: String, hu_max: String,
                        light_min: String, light_max: String,
                        soil_hu_min: String, soil_hu_max: String,
                        co2_min: String, co2_max: String,
                        completionHandler:  @escaping (_ result: Int, _ resultMessage: String, _ obj: AnyObject?) -> Void) {
        
        var strToken = "token="
        var strPlant = "plant="
        var strNewName = "new_name="
        var strTempMin = "temp_min=", strTempMax = "temp_max="
        var strHuMin = "hu_min=", strHuMax = "hu_max="
        var strLightMin = "light_min=", strLightMax = "light_max="
        var strSoiHuMin = "soi_hu_min=", strSoiHuMax = "soi_hu_max="
        var strCo2Min = "co2_min=", strCo2Max = "co2_max="
        
        if ( Util.str(token).count > 0) {
            strToken += token
        }
        if ( Util.str(plant).count > 0) {
            strPlant += plant
        }
        if ( Util.str(new_name).count > 0) {
            strNewName += new_name
        }
        if ( Util.str(temp_min).count > 0) {
            strTempMin += temp_min
        }
        if ( Util.str(temp_max).count > 0) {
            strTempMax += temp_max
        }
        if ( Util.str(hu_min).count > 0) {
            strHuMin += hu_min
        }
        if ( Util.str(hu_max).count > 0) {
            strHuMax += hu_max
        }
        if ( Util.str(light_min).count > 0) {
            strLightMin += light_min
        }
        if ( Util.str(light_max).count > 0) {
            strLightMax += light_max
        }
        if ( Util.str(soil_hu_min).count > 0) {
            strSoiHuMin += soil_hu_min
        }
        if ( Util.str(soil_hu_max).count > 0) {
            strSoiHuMax += soil_hu_max
        }
        if ( Util.str(co2_min).count > 0) {
            strCo2Min += co2_min
        }
        if ( Util.str(co2_max).count > 0) {
            strCo2Max += co2_max
        }
        
        
        
        var data = strToken + "&" + strPlant + "&" + strNewName + "&" + strTempMin + "&" + strTempMax
        data = data + "&" + strHuMin + "&" + strHuMax + "&" + strLightMin + "&" + strLightMax
        data = data + "&" + strSoiHuMin + "&" + strSoiHuMin + "&" + strCo2Min + "&" + strCo2Max
        
        HTTPUtil.async(url: urlPlantSetting, method: HTTPUtil.METHOD.POST, secTimeout: TIMEOUT, data: data, completionHandler: {data, response, err in
            
            if let error = err {
                print("error:", error)
                
                completionHandler(-1, error.localizedDescription, nil)
                
                
            }
            
            //                do {
            guard let data = data else { return }
            
            let plantSetting = JSONParser.parse(function: Function.plantSetting, data: data) as! PLANT_SETTING
            
            if (plantSetting.errorCode == "0") {
                
                completionHandler(0, "", plantSetting as AnyObject)
                
                // redirect to next page
                // print("succeed check token goto next page right now")
                
            } else {
                
                completionHandler(1, plantSetting.errorMessage, nil)
            }
            
            
            
        })
        
        
        
        
        
    }
    
    
}
