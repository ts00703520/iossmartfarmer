//
//  MainTableTemplateTableViewController.swift
//  argiculture
//
//  Created by Thanachai on 6/9/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import UIKit
import Firebase

class MainTableTemplateTableViewController: UITableViewController {

    @IBOutlet weak var butMenu: UIButton!
    @IBOutlet weak var butUser: UIButton!
    @IBOutlet weak var lblUser: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        lblUser.text = SeedUtil.userLogin
        //        lblFullname.text = NSLocalizedString("USERNAME", comment: "") + SeedUtil.userLogin
        //        lblUserId.text = NSLocalizedString("USERNAME", comment: "") + SeedUtil.userLogin
        
        SeedUtil.checkToken(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken, completionHandler: { result, message in
            if (result == 0) {
                print("view did appear check token success")
                
                //                self.refreshPlant() { () -> () in
                //                    self.refreshGetData()
                //                }
                
                
            } else if (result > 1 || result < 0) {
                DispatchQueue.main.async {
                    
                    let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                    self.show(vc as! UIViewController, sender: vc)
                }
            }
        })
        
        
        
        
        
    }
    
    
    @IBAction func logout(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("LOGOUT_TITLE", comment: ""), message: NSLocalizedString("LOGOUT_MESSAGE", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            
            SeedUtil.logout(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken,  completionHandler: {
                
                result, message in
                
                if ( result == 0 ) {
                    
                    NewPlantViewController.stopTimer()
                    
                    DispatchQueue.main.async {
                        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                        self.show(vc as! UIViewController, sender: vc)
                    }
                    
                } else {
                    
                    
                    DispatchQueue.main.async {
                        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                        self.show(vc as! UIViewController, sender: vc)
                    }
                    
                }
                
                
            })
            
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func menuClicked(_ sender: Any) {
    }


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
