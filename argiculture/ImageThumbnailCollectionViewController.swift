//
//  ImageThumbnailCollectionViewController.swift
//  argiculture
//
//  Created by thanachais on 14/3/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

private let reuseIdentifier = "imageCell"

class ImageThumbnailCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!

    @IBOutlet weak var lblNoImage: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    @IBOutlet weak var bodyViewNoImage: UIView!
    
    @IBOutlet weak var bodyViewWithImage: UIView!
    
    var noImages: Float = 0.0
    static var images:[String] = [String]()
    static var imageIndex = 0
    
    
    var tapGestureRecognizer: UITapGestureRecognizer?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        collectionView.backgroundView = bgImgView
        collectionView.delegate = self
        collectionView.dataSource = self
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
//        self.collectionView!.register(ImageCollectionCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//
//        if let flowLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//            flowLayout.itemSize = CGSize(width: self.collectionView.bounds.width / 2 , height: 120)
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        noImages = getTotalImages()
        collectionView.reloadData()
        if ( Shared.getData != nil ) {
            self.lblDate.text = NSLocalizedString("DATE", comment: "") + Shared.getData.data[0].data.datetime.prefix(10)
            
            self.lblTime.text = "" + Shared.getData.data[0].data.datetime.suffix(8)
            
            lblNoImage.text = NSLocalizedString("NO_IMAGE", comment: "")
            
            if noImages == 0 {
                bodyViewNoImage.isHidden = false
                bodyViewWithImage.isHidden = true
            } else {
                bodyViewNoImage.isHidden = true
                bodyViewWithImage.isHidden = false
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    

    // MARK: UICollectionViewDataSource

    func getTotalImages() -> Float {
        var noImages = 0
        
        if ( Shared.getData != nil && (Shared.getData.image?.count)! > 0 ) {
            for img in (Shared.getData?.image)! {
                if img.image_url.count > 0 {
                    noImages += 1
                }
            }
        }
        
        return Float(noImages)
    }
     func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        let nums = Int(ceil(noImages / 2.0 ))
        print("numberOfSections \(nums)")
        
        
        return nums
        
        
    }


     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
    
        if ( noImages == 1 ) {
            return 1
        } else {
            return 2
        }
        
    }

     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageCollectionCell
    
        // Configure the cell
        print("cellForItemAt section:\(indexPath.section) row:\(indexPath.row)")
        
        cell.lblInfo.isHidden = true
        
        
        var imageUrl = ""
        
        let index = (indexPath.section * 2) + (indexPath.row)
        
        imageUrl = Shared.getData.image![index].image_url 
        ImageThumbnailCollectionViewController.images.append(imageUrl)
        
        let url = URL(string: imageUrl)
        
        HTTPUtil.getData(from: url! ) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url!.lastPathComponent)
            print("Download Finished")
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if ( httpResponse.statusCode >= 200 && httpResponse.statusCode <= 200 ) {
                    DispatchQueue.main.async {
                        cell.img.image = UIImage(data: data)
                        cell.img.tag = index
                        cell.img.addGestureRecognizer(self.tapGestureRecognizer!)
                        cell.img.isUserInteractionEnabled = true
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        cell.img.image = UIImage(named: "default_image")
                        cell.img.tag = index
                        //                        cell.img.addGestureRecognizer(self.tapGestureRecognizer!)
                        //                        cell.img.isUserInteractionEnabled = true
                    }
                }
                
            }
            
            
            
            
            

        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if ( noImages == 1) {
            
//            let totalCellWidth = 80 * collectionView.numberOfItems(inSection: 0)
//            let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
            
            
            var top = collectionView.layer.frame.size.height / 2
            top = top - 200
//            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let leftInset = (collectionView.layer.frame.size.width - (collectionView.layer.frame.size.width-40)) / 2
            let rightInset = leftInset
            
            return UIEdgeInsets(top: top, left: leftInset, bottom: 0, right: rightInset)
        }
        
        else {
            
            
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
    }

    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  50
        let collectionViewSize = collectionView.frame.size.width - padding
        let collectionViewSizeHeight = (collectionView.frame.size.height - padding)/2
        
        if ( noImages == 1) {
           
            return CGSize(width: collectionViewSize, height: collectionViewSizeHeight + 50)
            
        } else {
            
            
            print("collectionViewSizeHeight: \(collectionViewSizeHeight)")
            
            return CGSize(width: collectionViewSize/2, height: collectionViewSizeHeight)
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("didSelectItemAt: \(indexPath)")
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
//        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        ImageThumbnailCollectionViewController.imageIndex = (tapGestureRecognizer.view?.tag)!
        print("imageTapped \(ImageThumbnailCollectionViewController.imageIndex)")
        
        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "ImageDetailViewController")
        //    let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainTabBarController")
        self.present(vc as! UIViewController, animated: true, completion: nil)
        
    }
    
    
    

    
    
    
    

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
