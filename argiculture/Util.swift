//
//  Util.swift
//  argiculture
//
//  Created by Thanachai on 3/8/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

extension Character {
    var asciiValue: Int {
        get {
            let s = String(self).unicodeScalars
            return Int(s[s.startIndex].value)
        }
    }
}

extension UIColor {
    
    class var dtacBlue: UIColor {
        return UIColor.init(red: 23/255.0, green: 141/255.0, blue: 245/255.0, alpha: 1.0)
    }
    class var dtacGreen: UIColor {
//        return UIColor.init(red: 102/255.0, green: 157/255.0, blue: 3/255.0, alpha: 1.0)
        return UIColor.init(red: 27/255.0, green: 191/255.0, blue: 80/255.0, alpha: 1.0)
    }
    class var dtacRed: UIColor {
        return UIColor.init(red: 212/255.0, green: 51/255.0, blue: 44/255.0, alpha: 1.0)
    }
    class var dtacDarkGrey: UIColor {
        return UIColor.init(red: 77/255.0, green: 77/255.0, blue: 77/255.0, alpha: 1.0)
    }
    class var dtacBrownGrey: UIColor {
        return UIColor.init(red: 179/255.0, green: 179/255.0, blue: 179/255.0, alpha: 1.0)
    }
    class var dtacAzure: UIColor {
        return UIColor.init(red: 17/255.0, green: 162/255.0, blue: 246/255.0, alpha: 1.0)
    }
    
}

// Screen width.
public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}

// Screen height.
public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}

public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

extension String {
    
    /// Create `NSData` from hexadecimal string representation
    ///
    /// This takes a hexadecimal representation and creates a String object from that. Note, if the string has any spaces, those are removed. Also if the string started with a '<' or ended with a '>', those are removed, too.
    ///
    /// - parameter encoding: The `NSStringCoding` that indicates how the binary data represented by the hex string should be converted to a `String`.
    ///
    /// - returns: `String` represented by this hexadecimal string.
    
    func stringFromHexadecimalStringUsingEncoding(encoding: String.Encoding) -> String? {
        if let data = dataFromHexadecimalString() {
            return String(data: data as Data, encoding: encoding)
        }
        
        return nil
    }
    
    func dataFromHexadecimalString() -> NSData? {
        let data = NSMutableData(capacity: self.count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, options: [], range: NSMakeRange(0, self.count)) { match, flags, stop in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString.withCString { strtoul($0, nil, 16) })
            data?.append([num], length: 1)
        }
        
        return data
    }
    
    
    /// Create hexadecimal string representation of String object.
    ///
    /// - parameter encoding: The NSStringCoding that indicates how the string should be converted to NSData before performing the hexadecimal conversion.
    ///
    /// - returns: String representation of this String object.
    
    //    func hexadecimalStringUsingEncoding(encoding: NSStringEncoding) -> String? {
    //        let data = dataUsingEncoding(NSUTF8StringEncoding)
    //        return data?.hexadecimalString()
    //    }
    
   
    
}

class Util: NSObject  {
    
    static func displayPendingAlert(uiViewController: UIViewController, message: String) -> UIAlertController  {
        
        let pending = UIAlertController(title: nil, message:  "\n" + NSLocalizedString(message, comment: "") , preferredStyle: .alert)
        
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.color = UIColor.dtacBlue
        
        pending.view.addSubview(indicator)
        
        let views = ["pending" : pending.view, "indicator" : indicator]
        var constraints = NSLayoutConstraint.constraints(withVisualFormat: "V:[indicator]-(50)-|", options: [], metrics: nil, views: views as [String : Any])
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|[indicator]|", options: [], metrics: nil, views: views as [String : Any])
    
        pending.view.addConstraints(constraints)
        
        indicator.isUserInteractionEnabled = false
        indicator.startAnimating()
        
        uiViewController.present(pending, animated: true, completion: nil)
        
        return pending
    }
    
    static func showMessage(view: UIViewController, title: String, message: String, alertTitle: String) {
        
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: alertTitle, style: UIAlertAction.Style.default, handler: nil))
            view.present(alert, animated: true, completion: nil)
        })
        
        
    }
    
    static func showMessage(view: UIViewController, title: String, message: String, alertTitle: String, finished: @escaping () -> Void) {
        
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: alertTitle, style: UIAlertAction.Style.default, handler:
                {
                    action in
                    finished()
            }
            ))
            view.present(alert, animated: true, completion: nil)
        })
        
         
    }
    
    
    static func str(_ string: String?) -> String {
        return (string != nil ? string! : "")
    }
    
    static func getLocale() -> String {
        return Locale.current.languageCode!
    }
    
    static func animateMyViews(viewToHide: UIView, viewToShow: UIView) {
        let animationDuration = 0.35
        
        UIView.animate(withDuration: animationDuration, animations: { () -> Void in
            viewToHide.transform = viewToHide.transform.scaledBy(x: 0.001, y: 0.001)
        }) { (completion) -> Void in
            
            viewToHide.isHidden = true
            viewToShow.isHidden = false
            
            viewToShow.transform = viewToShow.transform.scaledBy(x: 0.001, y: 0.001)
            
            UIView.animate(withDuration: animationDuration, animations: { () -> Void in
                viewToShow.transform = CGAffineTransform.identity
            })
            
            
        }
    }
    
    
    static func isNumberic(value: String) -> Bool {
        let num = Int(value)
        if num != nil {
            return true
        }
        else {
            return false
        }
    }
    
    static func getDateFormat(format: String) -> String {
        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = format
        
        return formatter.string(from: date)
    }
    
    
    static func sendNotification(grant: Bool, title: String, subTitle: String, message: String, interval: TimeInterval , repeat: Bool) {
        
        
         if #available(iOS 10.0, *) {
         if grant {
         //add notification code here
         
         //Set the content of the notification
         let content = UNMutableNotificationContent()
         content.title = title
         content.subtitle = subTitle
         content.body = message
            
         //                content.badge = NSNumber(100)
         content.sound = UNNotificationSound.default
         
         //Set the trigger of the notification -- here a timer.
         let trigger = UNTimeIntervalNotificationTrigger(
         timeInterval: interval,
         repeats: false)
         
         //Set the request for the notification from the above
         let request = UNNotificationRequest(
         identifier: UUID().uuidString,
         content: content,
         trigger: trigger
         )
        
         
         print("fire notification")
         //Add the notification to the currnet notification center
         UNUserNotificationCenter.current().add(
         request, withCompletionHandler: nil)
         
         }
         }
        
        
        
    }
    
    

}

extension NSObject {
    var className: String {
        return String(describing: type(of: self)).components(separatedBy: ".").last!
    }
    
    class var className: String {
        return String(describing: self).components(separatedBy: ".").last!
    }
}


extension UIImageView
{
    func addBlurEffect()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
    /// Remove UIBlurEffect from UIView
    func removeBlurEffect() {
        let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
        blurredEffectViews.forEach{ blurView in
            blurView.removeFromSuperview()
        }
    }
}
