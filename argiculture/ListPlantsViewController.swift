//
//  ListPlantsViewController.swift
//  argiculture
//
//  Created by thanachais on 30/4/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class ListPlantsViewController: UIViewController {

    var index:Int!
    
    @IBOutlet weak var tblPlant: UITableView!

    var selectPlantName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblPlant.dataSource = self
        tblPlant.delegate = self
        self.tblPlant.contentInset.bottom = 10
        self.tblPlant.separatorColor = UIColor.clear
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SeedUtil.listPlant(token: SeedUtil.token, plant: "", completionHandler: {
            result, message, obj in
            
            if ( result == 0) {
                
                Shared.listPlant = (obj as! LIST_PLANTS)
                DispatchQueue.main.async {
                    self.selectPlantName = Shared.getData.data[self.index].plant?.type
                    //                    self.tblPlant.reloadData()
                    
                    self.tblPlant.reloadData()
                }
                
                
            } else {
                
            }
            
        })
        
        
        
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListPlantsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ( Shared.listPlant != nil ) {
            return Shared.listPlant.list.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let selectPlantCell = tableView.dequeueReusableCell(withIdentifier: "selectPlantCell", for: indexPath) as! SelectPlantTableViewCell
        
        let plantName = Shared.listPlant.list.object(at: indexPath.row) as! String
        selectPlantCell.lblPlant.text = plantName
        selectPlantCell.imgPlant.image = UIImage(named: Shared.getPlantImageName(plantName))
        
        selectPlantCell.imgMark.isHidden = true
        //        if selectPlantName == Shared.getData.data[index].plant?.type {
        if selectPlantName == plantName {
            selectPlantCell.imgMark.isHidden = false
        }
        
        selectPlantCell.selectionStyle = .none
        
        return selectPlantCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("section: \(indexPath.section) row: \(indexPath.row)")
        clearValueAllCell(tableView: tableView, indexPath: indexPath)
        
        let selectPlantCell = tableView.cellForRow(at: indexPath) as! SelectPlantTableViewCell
        
        selectPlantCell.imgMark.isHidden = false
        selectPlantName = Shared.listPlant.list.object(at: indexPath.row) as? String
        
    }
    
    func clearValueAllCell(tableView: UITableView, indexPath: IndexPath) {
        //        var tmpIndexPath: IndexPath!
        //        tmpIndexPath = indexPath
        //        for row in 0...listPlant.list.count-1 {
        //
        //            tmpIndexPath.row = row
        //            let selectPlantCell = tableView.cellForRow(at: tmpIndexPath!.row) as! SelectPlantTableViewCell
        //            if ( selectPlantCell.imgMark.isHidden == false ) {
        //                selectPlantCell.imgMark.isHidden = true
        //            }
        //        }
        
        //        for row in 0..<tableView.numberOfRows(inSection: indexPath.section)-1 {
        //
        //            let indexPath = NSIndexPath(row: row, section: indexPath.section)
        //            let selectPlantCell = tableView.cellForRow(at: indexPath as IndexPath ) as! SelectPlantTableViewCell
        //            if ( selectPlantCell.imgMark.isHidden == false ) {
        //                selectPlantCell.imgMark.isHidden = true
        //            }
        //
        //
        //        }
        for cell in tableView.visibleCells {
            
            let selectPlantCell = cell as! SelectPlantTableViewCell
            if ( selectPlantCell.imgMark.isHidden == false ) {
                selectPlantCell.imgMark.isHidden = true
            }
            
        }
        
    }
}
