//
//  SensorControlTableViewCell.swift
//  argiculture
//
//  Created by thanachais on 5/3/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//
import Foundation
import UIKit

class SensorControlTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblControlType: UILabel!
    
    @IBOutlet weak var imgBg: UIImageView!
    
    @IBOutlet weak var imgSwitch: UIImageView!
    
    @IBOutlet weak var imgControl: UIImageView!
    
}
