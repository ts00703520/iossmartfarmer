//
//  MenuTableViewController.swift
//  argiculture
//
//  Created by Thanachai on 5/18/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import UIKit
import Firebase

class MenuTableViewController: UITableViewController {

    
    @IBOutlet weak var lblMain: UILabel!
    @IBOutlet weak var lblSettingNoti: UILabel!
    @IBOutlet weak var lblPlant: UILabel!
    @IBOutlet weak var lblLogout: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.title = NSLocalizedString("MENU", comment: "")
        self.lblSettingNoti.text = NSLocalizedString("MENU_EDIT_NOTIFICATION", comment: "")
        self.lblPlant.text = NSLocalizedString("MENU_EDIT_TRANSMITTER", comment: "")
        self.lblLogout.text = NSLocalizedString("MENU_LBL_LOGOUT", comment: "")
        self.lblAbout.text = NSLocalizedString("MENU_LBL_ABOUT", comment: "")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.row == 0) {
            
            NewPlantViewControllerV2.stopTimer()
            
            DispatchQueue.main.async {
                let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: Shared.PLAN_VIEW_CONTROLLER_NAME)
                self.show(vc as! UIViewController, sender: vc)
            }

        
      /*  } else if (indexPath.row == 1) {
            
            NSLog("setting")
            
            NewPlantViewController.stopTimer()
            
            DispatchQueue.main.async {
                let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "SettingsViewController")
                self.show(vc as! UIViewController, sender: vc)
            } */
            
        } else if (indexPath.row == 1) {
            
            NSLog("setting")
            
            NewPlantViewControllerV2.stopTimer()
            
            DispatchQueue.main.async {
//                let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "SettingsViewController")
                let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "SettingNotificationViewController")
                self.show(vc as! UIViewController, sender: vc)
            }
        } else if (indexPath.row == 2) {
            
            NSLog("plant setting")
            
            NewPlantViewControllerV2.stopTimer()
            
            DispatchQueue.main.async {
                let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "EditPlantViewController")
                self.show(vc as! UIViewController, sender: vc)
            }
            
            
        } else if (indexPath.row == 3) {
            
            
            let alert = UIAlertController(title: NSLocalizedString("LOGOUT_TITLE", comment: ""), message: NSLocalizedString("LOGOUT_MESSAGE", comment: ""), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                
                Util.displayPendingAlert(uiViewController: self, message: "LOADING")
                
                SeedUtil.logout(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken,  completionHandler: {
                    
                    result, message in
                    
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: {
                            
                            if ( result == 0 ) {
                                                    
                                                    NewPlantViewControllerV2.stopTimer()
                                                    
                            //                        self.dismiss(animated: false, completion: {
                            //
                            //
                            //                        })
                                                    DispatchQueue.main.async {
                                                        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                                                        self.show(vc as! UIViewController, sender: vc)
                                                    }


                                                    
                                                } else {
                                                    
                                                    
                            //                        DispatchQueue.main.async {
                            //                            let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                            //                            self.show(vc as! UIViewController, sender: vc)
                            //                        }
                                                    
                                                }
                            
                            
                        })
                    }
                    
                    
                    
                })
                
                
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        } else if (indexPath.row == 4) {
            
            let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            let message = NSLocalizedString("ABOUT_MESSAGE", comment: "").replacingOccurrences(of: "{0}", with: version)
            
            let alert = UIAlertController(title: NSLocalizedString("ABOUT_TITLE", comment: ""), message: message, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else {
                return UITableViewCell(style: .default, reuseIdentifier: "cell")
            }
            return cell
        }()
        
//        cell.textLabel?.text = anyArray[indexPath.row]
        
        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
