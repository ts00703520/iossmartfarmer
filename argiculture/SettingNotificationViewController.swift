//
//  SettingNotificationViewController.swift
//  argiculture
//
//  Created by thanachais on 25/4/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class SettingNotificationViewController: UIViewController {

//    let hours = ["", "", "1.00", "2.00", "3.00", "4.00", "5.00", "6.00", "7.00", "8.00", "9.00", "10.00", "11.00", "12.00", "", ""]
//    let hours24 = ["", "", "13.00", "14.00", "15.00", "16.00", "17.00", "18.00", "19.00", "20.00", "21.00", "22.00", "23.00", "24.00",  "", "" ]
    let hours = ["1.00", "2.00", "3.00", "4.00", "5.00", "6.00", "7.00", "8.00", "9.00", "10.00", "11.00", "12.00"]
    let hours24 = ["13.00", "14.00", "15.00", "16.00", "17.00", "18.00", "19.00", "20.00", "21.00", "22.00", "23.00", "24.00"]
    
//    let period = [
//                    ["", "", "morning", "evening", "", "" ],
//                    ["", "", "เช้า","บ่าย", "", "" ]
//                ]
    let period = [
        ["morning", "evening"],
        ["เช้า","บ่าย"]
    ]
    
    var settings: SETTINGS!
    var onOffState = ""
    var startStopState = "start"
    var periodState = "morning"
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNotification: UILabel!
    
    @IBOutlet weak var lblOnOff: UILabel!
    @IBOutlet weak var imgOnOff: UIImageView!
    
    @IBOutlet weak var lblStartHour: UILabel!
    @IBOutlet weak var lblStopHour: UILabel!
    
    @IBOutlet weak var butStartHour: UIButton!
    
    @IBOutlet weak var butStopHour: UIButton!
    
    @IBOutlet weak var lblHour: UILabel!
    
    @IBOutlet weak var lblPeriod: UILabel!
    
    @IBOutlet weak var timeTableView: UIView!
    
    @IBOutlet weak var tblHour: UITableView!
    
    @IBOutlet weak var tblPeriod: UITableView!
    
    @IBOutlet weak var butBack: UIButton!
    
    @IBOutlet weak var butConfirm: UIButton!
    
    @IBOutlet weak var pickerHour: UIPickerView!
    var oldView: UIView!
    var selectedView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.tblHour.dataSource = self
//        self.tblHour.delegate = self
//        self.tblPeriod.dataSource = self
//        self.tblPeriod.delegate = self
        tblHour.isHidden = true
        tblPeriod.isHidden = true
        
        
        self.pickerHour.delegate = self
        self.pickerHour.dataSource = self
        
        
        
        self.tblHour.separatorStyle = .none
        self.tblPeriod.separatorStyle = .none
        
        lblTitle.text = NSLocalizedString("SETT_NOTI_LBL_TITLE", comment: "")
        
        
        lblNotification.text = NSLocalizedString("SETT_NOTI_LBL_NOTI", comment: "")
        lblOnOff.text = NSLocalizedString("SETT_NOTI_LBL_ON", comment: "")
        lblStartHour.text = NSLocalizedString("SETT_NOTI_LBL_START_HOUR", comment: "")
        lblStopHour.text = NSLocalizedString("SETT_NOTI_LBL_STOP_HOUR", comment: "")
        lblHour.text = NSLocalizedString("SETT_NOTI_LBL_HOUR", comment: "")
        lblPeriod.text = NSLocalizedString("SETT_NOTI_LBL_PERIOD", comment: "")
        
        butStartHour.setTitle(NSLocalizedString("SETT_NOTI_BUT_START_HOUR", comment: ""), for: .normal)
        butStopHour.setTitle(NSLocalizedString("SETT_NOTI_BUT_STOP_HOUR", comment: ""), for: .normal)
        
        butBack.setTitle(NSLocalizedString("SETT_NOTI_BUT_BACK", comment: ""), for: .normal)
        butConfirm.setTitle(NSLocalizedString("SETT_NOTI_BUT_CONFIRM", comment: ""), for: .normal)
        
        
        timeTableView.layer.borderColor = UIColor.dtacBlue.cgColor
        timeTableView.layer.borderWidth = 2
        timeTableView.layer.cornerRadius = 10
        timeTableView.layer.masksToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchOnOff(tapGestureRecognizer:)))
        imgOnOff.isUserInteractionEnabled = true
        imgOnOff.addGestureRecognizer(tapGestureRecognizer)
        
        
        displayStartStopChange(state: startStopState)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Call SeedUtil get notification setting
        SeedUtil.saveSettings(token: SeedUtil.token, enableNoti: "", startTime: "", endTime: "", fcm: "", completionHandler: {
            
            result, message, obj in
            
            if ( result == 0 ) {
                
                self.settings = (obj as! SETTINGS)
                
                if ( self.settings.errorCode == "0") {
                    
                    
                    DispatchQueue.main.async {
                        
                        if (self.settings.enablenoti.count > 0 ) {
                            
                            self.displayStateChange(state: self.settings.enablenoti)
                            
                        }
                        
                        
                        if (self.settings.starttime.count > 0 ) {
                            
                            self.lblStartHour.text = self.settings.starttime + ".00"
                        }
                        
                        if (self.settings.endtime.count > 0 ) {
                            
                            self.lblStopHour.text = self.settings.endtime + ".00"
                            
                        }
                        
                        var moveRowHour = 0
                        var moveRowPeriod = 0
                        if ( Int(self.settings.starttime)! <= 12 ) {
                            
                            moveRowHour = Int(self.settings.starttime)! + 1
                            moveRowPeriod = 2
                            
                            
                        } else {
                            
                            moveRowHour = Int(self.settings.starttime)! - 12
                            moveRowPeriod = 3
                            self.periodState = "evening"
                        }
                        
//                        var indexPath = NSIndexPath(item: moveRowHour, section: 0)
//                        self.tblHour.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.middle, animated: true)
//                        
//                        indexPath = NSIndexPath(item: moveRowPeriod, section: 0)
//                        self.tblPeriod.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.middle, animated: true)
                        
                        
                        self.displayStateChange(state: self.settings.enablenoti)
                    }
                    
                }
                
            } else {
                
                // alert message
                Util.showMessage(view: self, title: NSLocalizedString("ERROR_TITLE", comment: ""), message: NSLocalizedString(message, comment: "") , alertTitle: NSLocalizedString("ERROR_TITLE", comment: ""))
            }
            
        })
    }
    
    
    @IBAction func setHour(_ sender: Any) {
        
        startStopState = "start"
        displayStartStopChange(state: startStopState)
    }
    
    @IBAction func setPeriod(_ sender: Any) {
        startStopState = "stop"
        displayStartStopChange(state: startStopState)
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func confirm(_ sender: Any) {
        
        
        
        SeedUtil.saveSettings(token: SeedUtil.token, enableNoti: onOffState,
                              startTime: lblStartHour.text!.replacingOccurrences(of: ".00", with: ""),
                              endTime: lblStopHour.text!.replacingOccurrences(of: ".00", with: ""),
                              fcm: SeedUtil.tokenFcm, completionHandler: {
                            
            result, message, obj in
            
            if (result == 0) {
                print("save settings success")
                
                DispatchQueue.main.async {
                    
//                    let alert = UIAlertController(title: NSLocalizedString("SUCCEED", comment: ""), message: NSLocalizedString("SAVE_SETTINGS_SUCCEED", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
//                    let action = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.default, handler: { action in
//
//                        self.navigationController?.popToRootViewController(animated: true)
//
//                    })
//                    alert.addAction(action)
//                    self.present(alert, animated: true, completion: {
//                    })
                    
                    Util.showMessage(view: self, title: NSLocalizedString("SUCCEED", comment: ""), message:  NSLocalizedString("SAVE_SETTINGS_SUCCEED", comment: ""), alertTitle: NSLocalizedString("OK", comment: ""), finished: {

                        self.navigationController?.popToRootViewController(animated: true)

                    })
                }
                
                
                
                
            } else if (result > 1 || result < 0) {
                
                DispatchQueue.main.async {
                    Util.showMessage(view: self, title: NSLocalizedString("ERROR_TITLE", comment: ""), message: NSLocalizedString(message, comment: "") , alertTitle: NSLocalizedString("OK", comment: ""))
                }
                
                
            }
            
        })
        
        
    }
    
    func displayStateChange(state: String) {
        switch state {
        case "0":
            self.imgOnOff.image = Shared.imgOff
            self.lblOnOff.text =  NSLocalizedString("SETT_NOTI_LBL_OFF", comment: "")
            self.onOffState = "0"
            break
        default:
            self.imgOnOff.image = Shared.imgOn
            self.lblOnOff.text =  NSLocalizedString("SETT_NOTI_LBL_ON", comment: "")
            self.onOffState = "1"
            break
        }
    }
    
    func displayStartStopChange(state: String) {
        switch state {
        case "start":
            butStartHour.setBackgroundImage(UIImage(named: "button_fill_green_check"), for: .normal)
            butStopHour.setBackgroundImage(UIImage(named: "button_fill_green"), for: .normal)
            
            break
        default:
            butStartHour.setBackgroundImage(UIImage(named: "button_fill_green"), for: .normal)
            butStopHour.setBackgroundImage(UIImage(named: "button_fill_green_check"), for: .normal)
            
            break
        }
    }
    
    
    @objc func switchOnOff(tapGestureRecognizer: UITapGestureRecognizer) {
        
//        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        
        if ( self.onOffState == "0") {
            self.onOffState = "1"
        } else {
            self.onOffState = "0"
        }
        
        displayStateChange(state: self.onOffState)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SettingNotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row=0
        
        switch tableView.tag {
        case 10:
            row = hours.count
            break
        default:
            row = period[0].count
            break
        }
        return row
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
    
        switch tableView.tag {
        case 10:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellHour", for: indexPath)
            cell.textLabel!.text = hours[indexPath.row]
            
            break
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellPeriod", for: indexPath)
            
            if String(Locale.preferredLanguages[0].prefix(2)) == "th" {
                cell.textLabel!.text = period[1][indexPath.row]
            } else {
                cell.textLabel!.text = period[0][indexPath.row]
            }
            
            
            break
        }
        cell.textLabel?.textAlignment = .center
        cell.textLabel!.font = UIFont(name: "DTAC 2017 Regular", size: 14.0)
        cell.textLabel!.textColor = .dtacAzure
        cell.selectionStyle = .none
        
        return cell
    }
    
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//
//        CGFloat cellHeight = 44.0f;
//        NSInteger cellIndex = floor(targetContentOffset->y / cellHeight);
//
//        // Round to the next cell if the scrolling will stop over halfway to the next cell.
//        if ((targetContentOffset->y - (floor(targetContentOffset->y / cellHeight) * cellHeight)) > cellHeight) {
//            cellIndex++;
//        }
//
//        // Adjust stopping point to exact beginning of cell.
//        targetContentOffset->y = cellIndex * cellHeight;
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if ( scrollView == tblHour && tblHour.numberOfRows(inSection: 0) >= 5) {
            
            displayHourHighlight()
        } else if ( scrollView == tblPeriod ) {
            displayPeriodHighlight()
            
            let cell = tblHour.visibleCells[2]
            let row = (tblHour.indexPath(for: cell)?.row)!
            
            displayNumber(row: row)
            /*
            if (periodState == "evening" ) {
                
                lblStartHour.text = hours24[row]
            } else {
                lblStartHour.text = hours[row]
            }
            */
        } 
    }
    
    func displayPeriodHighlight() {
        let middle = 2
        var loop = 0
        for cell in tblPeriod.visibleCells {
            if loop == middle {
                cell.backgroundColor = .dtacBrownGrey
                cell.textLabel?.textColor = .white
                
                periodState = "evening"
                for i in 0..<period.count {
                    if period[i][2] == cell.textLabel?.text {
                        periodState = "morning"
                    }
                }
                
            } else {
                cell.backgroundColor = .white
                cell.textLabel?.textColor = .dtacAzure
            }
            loop = loop + 1
        }
        
        
    }
    
    func displayHourHighlight() {
        
        let middle = 2
        var loop = 0
        
        for cell in tblHour.visibleCells {
            if loop == middle {
                cell.backgroundColor = .dtacBrownGrey
                cell.textLabel?.textColor = .white
                let row = (tblHour.indexPath(for: cell)?.row)!
                
                displayNumber(row: row)
                /*
                if startStopState == "start" {
                    
                    if (periodState == "evening" ) {
                        
                        self.lblStartHour.text = hours24[row]
                    } else {
                        self.lblStartHour.text = hours[row]
                    }
                    
                    
                } else {
                    
                    if (periodState == "evening" ) {
                        
                        self.lblStopHour.text = hours24[row]
                    } else {
                        self.lblStopHour.text = hours[row]
                    }
                    
                    
                }
                */
                
            } else {
                cell.backgroundColor = .white
                cell.textLabel?.textColor = .dtacAzure
            }
            loop = loop + 1
            
        }
        
        
    }
    
    
    
}
extension SettingNotificationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        if ( numberOfComponents(in: pickerView) == 0) {
        if component == 0 {
            return hours.count
        } else {
            return period[0].count
        }
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//
//
//
//    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if component == 0 {
            print("pickerview didselectrow \(row)")
            
            displayNumber(row: row)
            
        } else {
            if row == 0 {
                periodState = "morning"
            } else {
                periodState = "evening"
            }
            pickerView.reloadComponent(0)
        }
    }
    
//    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//
//        var titleData = ""
////        let color = (row == pickerView.selectedRow(inComponent: component)) ? UIColor.green : UIColor.black
//        let color = UIColor.dtacAzure
//
//        if component == 0 {
//            titleData = hours[row]
//        } else {
//            if String(Locale.preferredLanguages[0].prefix(2)) == "th" {
//                titleData = period[1][row]
//            } else {
//                titleData = period[0][row]
//            }
//        }
//
//        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedString.Key.foregroundColor: color
//
//            ])
//
////        cell.textLabel?.textAlignment = .center
////        cell.textLabel!.font = UIFont(name: "DTAC 2017 Regular", size: 14.0)
////        cell.textLabel!.textColor = .dtacAzure
//
//        return myTitle
//
//
//    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel = view as? UILabel;
        
        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()
            
            pickerLabel?.font = UIFont(name: "DTAC 2017 Regular", size: 18.0)!
            pickerLabel?.textAlignment = NSTextAlignment.center
            pickerLabel?.textColor = UIColor.dtacAzure
        }
        
        var titleData = ""
        
        if component == 0 {
            if periodState == "morning" {
                titleData = hours[row]
            } else {
                titleData = hours24[row]
            }
        } else {
            if String(Locale.preferredLanguages[0].prefix(2)) == "th" {
                titleData = period[1][row]
            } else {
                titleData = period[0][row]
            }
        }
        
        pickerLabel?.text = titleData
        
        displayNumber(row: row)
        
        return pickerLabel!;
        
        
    }
    
}
extension SettingNotificationViewController {
    
    func displayNumber(row: Int) {
        if startStopState == "start" {
            
            if (periodState == "evening" ) {
                
                self.lblStartHour.text = hours24[row]
            } else {
                self.lblStartHour.text = hours[row]
            }
            
            
        } else {
            
            if (periodState == "evening" ) {
                
                self.lblStopHour.text = hours24[row]
            } else {
                self.lblStopHour.text = hours[row]
            }
            
            
        }
    }
}

