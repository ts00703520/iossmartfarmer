//
//  SelectPlantViewController.swift
//  argiculture
//
//  Created by thanachais on 22/4/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class SelectPlantViewController: UIViewController {

    var index: Int = 0
    var selectSettingIndex = 0
    
    @IBOutlet weak var butBack: UIButton!
    
    @IBOutlet weak var butConfirm: UIButton!
    
    
    @IBOutlet weak var butPlant: UIButton!
    @IBOutlet weak var butMinMax: UIButton!
    
    
    @IBOutlet weak var containerTransmitter: UIView!
    
    @IBOutlet weak var containerMinMax: UIView!
    
    var containerVC: TransmitterHeaderViewController!
    var listPlantsViewController: ListPlantsViewController!
    var setMinMaxViewController: SetMinMaxViewController!
    var selectPlantName: String!
    
    @IBOutlet weak var tblPlant: UITableView!
    
//    var listPlant: LIST_PLANTS!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        tblPlant.dataSource = self
//        tblPlant.delegate = self
//        self.tblPlant.contentInset.bottom = 10
//        self.tblPlant.separatorColor = UIColor.clear
        
        butBack.setTitle(NSLocalizedString("SELECT_PLANT_BUT_BACK", comment: ""), for: .normal)
        butConfirm.setTitle(NSLocalizedString("SELECT_PLANT_BUT_CONFIRM", comment: ""), for: .normal)
        butPlant.setTitle(NSLocalizedString("SELECT_PLANT_BUT_TRASMITTER", comment: ""), for: .normal)
        butMinMax.setTitle(NSLocalizedString("SELECT_PLANT_BUT_MIN_MAX", comment: ""), for: .normal)
        
//        butMinMax.isHidden = true
        
//        butBack.setNeedsDisplay()
//        butConfirm.setNeedsDisplay()
        
        containerVC.display(index: index)
        self.butPlantClick(self)
//        selectPlantName = listPlant.list.object(at: index) as! String
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        butBack.setTitle(NSLocalizedString("SELECT_PLANT_BUT_BACK", comment: ""), for: .normal)
        butConfirm.setTitle(NSLocalizedString("SELECT_PLANT_BUT_CONFIRM", comment: ""), for: .normal)
//        butBack.setNeedsDisplay()
//        butConfirm.setNeedsDisplay()
        
        
        
        
    }
    
   
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TransmitterHeaderView" {
            if let vc = segue.destination as? TransmitterHeaderViewController {
                self.containerVC = vc
            }
        }
        else if segue.identifier == "ListPlantsView" {
            if let vc = segue.destination as? ListPlantsViewController {
                vc.index = self.index
                listPlantsViewController = vc
            }
        }
        else if segue.identifier == "SetMinMaxView" {
            if let vc = segue.destination as? SetMinMaxViewController {
                setMinMaxViewController = vc
                setMinMaxViewController.index = 0
            }
        }
    }
    
    
    
    @IBAction func goBack(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirm(_ sender: Any) {
        
        // Call SeedUtil API
        
        DispatchQueue.main.async {
            _ = Util.displayPendingAlert(uiViewController: self, message: "LOADING")
        }
        
        
        if ( selectSettingIndex == 0) {
            
            SeedUtil.setPlant(token: SeedUtil.token, serial: Shared.getData.data[index].sn, plant: listPlantsViewController.selectPlantName , completionHandler: {
                result, message, obj in
                
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: {
                        
                        if ( result == 0) {
                            
                            self.navigationController?.popToRootViewController(animated: true)
                            
                            
                        } else {
                            
                            print("setPlant failed \(message)")
                        }
                        
                    })
                }
                
                
                
            })
        } else {
            
            
            let plantSetting = setMinMaxViewController.savePlantSetting!
            
            SeedUtil.plantSetting(token: SeedUtil.token, plant: plantSetting.type, new_name: plantSetting.type,
                                  temp_min: plantSetting.temp_min,
                                  temp_max: plantSetting.temp_max,
                                  hu_min: plantSetting.hu_min,
                                  hu_max: plantSetting.hu_max,
                                  light_min: plantSetting.light_min,
                                  light_max: plantSetting.light_max,
                                  soil_hu_min: plantSetting.soi_hu_min,
                                  soil_hu_max: plantSetting.soi_hu_max,
                                  co2_min: plantSetting.co2_min!,
                                  co2_max: plantSetting.co2_max!,
                completionHandler: { result, message, obj in
                
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: {
                        
                        if ( result == 0) {
                            
                            self.navigationController?.popToRootViewController(animated: true)
                            
                            
                        } else {
                            
                            print("setPlant failed \(message)")
                        }
                        
                    })
                }
                
                
            })
            
            
        }
        
        
    
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func butPlantClick(_ sender: Any) {
        containerTransmitter.isHidden = false
        containerMinMax.isHidden = true
        
        butPlant.setBackgroundImage(UIImage(named: "button_fill_green_check"), for: .normal)
        butMinMax.setBackgroundImage(UIImage(named: "button_fill_green"), for: .normal)
        selectSettingIndex = 0
    }
    
    @IBAction func butMinMaxClick(_ sender: Any) {
        containerTransmitter.isHidden = true
        containerMinMax.isHidden = false
        
        butPlant.setBackgroundImage(UIImage(named: "button_fill_green"), for: .normal)
        butMinMax.setBackgroundImage(UIImage(named: "button_fill_green_check"), for: .normal)
        selectSettingIndex = 1
    }
    
}

extension SelectPlantViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ( Shared.listPlant != nil ) {
            return Shared.listPlant.list.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let selectPlantCell = tableView.dequeueReusableCell(withIdentifier: "selectPlantCell", for: indexPath) as! SelectPlantTableViewCell
        
        let plantName = Shared.listPlant.list.object(at: indexPath.row) as! String
        selectPlantCell.lblPlant.text = plantName
        selectPlantCell.imgPlant.image = UIImage(named: Shared.getPlantImageName(plantName))
        
        selectPlantCell.imgMark.isHidden = true
//        if selectPlantName == Shared.getData.data[index].plant?.type {
        if selectPlantName == plantName {
            selectPlantCell.imgMark.isHidden = false
        }
        
        selectPlantCell.selectionStyle = .none
        
        return selectPlantCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("section: \(indexPath.section) row: \(indexPath.row)")
        clearValueAllCell(tableView: tableView, indexPath: indexPath)
        
        let selectPlantCell = tableView.cellForRow(at: indexPath) as! SelectPlantTableViewCell
        
        selectPlantCell.imgMark.isHidden = false
        selectPlantName = Shared.listPlant.list.object(at: indexPath.row) as? String
        
    }
    
    func clearValueAllCell(tableView: UITableView, indexPath: IndexPath) {
//        var tmpIndexPath: IndexPath!
//        tmpIndexPath = indexPath
//        for row in 0...listPlant.list.count-1 {
//
//            tmpIndexPath.row = row
//            let selectPlantCell = tableView.cellForRow(at: tmpIndexPath!.row) as! SelectPlantTableViewCell
//            if ( selectPlantCell.imgMark.isHidden == false ) {
//                selectPlantCell.imgMark.isHidden = true
//            }
//        }
        
//        for row in 0..<tableView.numberOfRows(inSection: indexPath.section)-1 {
//
//            let indexPath = NSIndexPath(row: row, section: indexPath.section)
//            let selectPlantCell = tableView.cellForRow(at: indexPath as IndexPath ) as! SelectPlantTableViewCell
//            if ( selectPlantCell.imgMark.isHidden == false ) {
//                selectPlantCell.imgMark.isHidden = true
//            }
//
//
//        }
        for cell in tableView.visibleCells {
            
            let selectPlantCell = cell as! SelectPlantTableViewCell
            if ( selectPlantCell.imgMark.isHidden == false ) {
                selectPlantCell.imgMark.isHidden = true
            }
            
        }
        
    }
}
