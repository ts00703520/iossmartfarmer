//
//  NewPlantViewController.swift
//  argiculture
//
//  Created by Thanachai on 3/28/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import Foundation
import UIKit
import SideMenu
import Firebase


class NewPlantViewControllerV2: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var bgImgView: UIImageView!
    @IBOutlet weak var tblSensor: UITableView!

    @IBOutlet weak var dateTimeView: UIView!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblFullname: UILabel!
    
    @IBOutlet weak var lblUserId: UILabel!
    @IBOutlet weak var lblPlant: UILabel!
    
    @IBOutlet weak var pickerPlant: UIPickerView!
    
    @IBOutlet weak var lblDisplayPlant: UILabel!
    @IBOutlet weak var butEdit: UIButton!
    
    @IBOutlet weak var houseContainerView: UIView!
    
    @IBOutlet weak var houseConstraintHeight: NSLayoutConstraint!
    
    @IBOutlet weak var houseDownConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var bodyContraintHeight: NSLayoutConstraint!
    
   
    @IBOutlet weak var datetimeConstraintTop: NSLayoutConstraint!
    @IBOutlet weak var tblSensorContraintTop: NSLayoutConstraint!
    
    static var timer: Timer? = nil
    var listPlant: LIST_PLANTS!
    var hu_min = 0
    var hu_max = 0
    var soil_min = 0
    var soil_max = 0
    var light_min = 0
    var light_max = 0
    var temp_min = 0
    var temp_max = 0
    var co2_min = 0
    var co2_max = 0
    
    let cellSpacingHeight: CGFloat = 5
    
    var plantArray: NSArray = []
    
    
    typealias FINISHED_REFRESH_PLANT = () -> ()
    var index = 0
    
    let HEADER_HEIGHT = 50
    let BODY_HEIGHT = 90
    let IO_CELL_HEIGHT = 90
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(NewPlantViewControllerV2.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.dtacBlue
        
        return refreshControl
    }()
    
    var houseTableViewController: HouseTableViewController?
    
    // MARK: View Controller
    override func viewDidLoad() {
        
        bgImgView.addBlurEffect()
        
        tblSensor.delegate = self
        tblSensor.dataSource = self
        
//        houseTableViewController = self.storyboard!.instantiateViewController(withIdentifier: "HouseTableViewController") as! HouseTableViewController
        
        
        pickerPlant.delegate = self
        pickerPlant.dataSource = self
        
//        let backgroundImage = UIImage(named: "background_table.png")
//        let imageView = UIImageView(image: backgroundImage)
//        imageView.contentMode = .scaleAspectFit
//
//
        self.tblSensor.backgroundView = bgImgView
        self.tblSensor.addSubview(self.refreshControl)
//        self.tblSensor.contentInset.bottom = self.tabBarController?.tabBar.frame.height ?? 0
        self.tblSensor.contentInset.bottom = 10
        self.tblSensor.separatorColor = UIColor.clear
        self.tblSensor.allowsSelection = false
        
        dateTimeView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.houseConstraintHeight.constant = 0
        self.bodyContraintHeight.constant = 0
        self.bodyView.isHidden = true
        self.datetimeConstraintTop.constant = 0
        
        /*
        SeedUtil.listHouse(token: SeedUtil.token, completionHandler: { result, message, object in
            
            
            if (result == 0) {
                let obj = object as! LIST_HOUSE
                
                for house in obj.house {
                    print("house: \(house.name)")
                }
                
                if obj.house.count > 0 {
                    Shared.HOUSE_ROW_NO = obj.house.count
                    DispatchQueue.main.async {
                        self.houseConstraintHeight.constant = CGFloat(Shared.HOUSE_HEIGHT + Shared.HOUSE_ARROW_HEIGHT)
                        
                        self.houseTableViewController?.reloadData()
                        self.houseDownConstraint.constant = Shared.HOUSE_ARROW_HEIGHT_HALF * -1
                        
                    }
                    
                }
                
            } else {
                print("listHouse error \(message)")
                
            }
        })
        */
        
        self.lblPlant.text = NSLocalizedString("PLANTS", comment: "")
        self.lblDate.text = NSLocalizedString("DATE", comment: "")
        self.lblTime.text = NSLocalizedString("TIME", comment: "")
        
        if Shared.getData != nil && Shared.getData.data.count > 0 {
            self.lblDate.text = NSLocalizedString("DATE", comment: "") + Shared.getData.data[0].data.datetime.prefix(10)
            
            self.lblTime.text = NSLocalizedString("TIME", comment: "") + Shared.getData.data[0].data.datetime.suffix(8)
            
        }
        self.lblDate.numberOfLines = 0
        self.lblDate.sizeToFit()
        self.lblDate.setNeedsDisplay()
        self.lblTime.numberOfLines = 0
        self.lblTime.sizeToFit()
        self.lblTime.setNeedsDisplay()
        
        print("view will Appear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
//        lblUser.text = SeedUtil.userLogin
//        lblFullname.text = NSLocalizedString("USERNAME", comment: "") + SeedUtil.userLogin
//        lblUserId.text = NSLocalizedString("USERNAME", comment: "") + SeedUtil.userLogin
        
        SeedUtil.checkToken(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken, completionHandler: { result, message in
            if (result == 0) {
                print("view did appear check token success")
                
                self.refreshPlant() { () -> () in
                    self.refreshGetData()
                }
                
                self.startTimer()
                
            } else {
                
                NewPlantViewControllerV2.stopTimer()
                
                DispatchQueue.main.async {
                    
                    
                    let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                    self.show(vc as! UIViewController, sender: vc)
                }
            }
        })
        
        if ( Shared.HOUSE_ROW_NO > 0) {
            
            
        }
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NewPlantViewControllerV2.stopTimer()
        
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        
                present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)
        
        // Similarly, to dismiss a menu programmatically, you would do this:
        //        dismiss(animated: true, completion: nil)
        
        // For Swift 2.3 (no longer maintained), use:
        // presentViewController(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
   
    
    //MARK: Delegates
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return plantArray[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        print("select plant is: \(row) \(self.plantArray[row])")
        
        SeedUtil.listPlant(token: SeedUtil.token, plant: self.plantArray[row] as! String, completionHandler: {
            
            result, message, obj in
            
            if ( result == 0 ) {
                
                //                let listPlant = obj as! LIST_PLANTS
                self.listPlant = (obj as! LIST_PLANTS)
                
                self.plantArray = self.listPlant.list as! NSArray
                let selectedItem = self.findSelectedPlant(plant: self.listPlant.plant)
                
                DispatchQueue.main.async {
                    self.pickerPlant.reloadAllComponents()
                    //                    if ( self.pickerPlant.numberOfComponents ) {
                    //                        self.pickerPlant.selectedRow(inComponent: selectedItem)
                    //                    }
                    self.pickerPlant.selectRow(selectedItem, inComponent: 0, animated: false)
//                    self.lblDisplayPlant.text = self.listPlant.plant
                    
                }
                
                
                self.refreshGetData()
                
                
                print("list array value is \(self.plantArray)")
                
                
            } else {
                
                print("error listplan \(message)")
            }
            
            
        })
        
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as! UILabel?
        //        DispatchQueue.main.async {
        
        if view == nil {  //if no label there yet
            pickerLabel = UILabel()
            //color the label's background
            //            let hue = CGFloat(row)/CGFloat(self.plantArray.count)
            //            pickerLabel?.backgroundColor = UIColor(hue: hue, saturation: 1.0, brightness: 1.0, alpha: 1.0)
        }
        
        let titleData = self.plantArray[row]
        let myTitle = NSAttributedString(string: titleData as! String, attributes: [NSAttributedString.Key.font:UIFont(name: "DTAC 2017 Regular", size: 16.0)!,NSAttributedString.Key.foregroundColor:UIColor.white])
        pickerLabel?.attributedText = myTitle
        pickerLabel?.textAlignment = .center
        //            }
        return pickerLabel!
    }
    
    
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return plantArray.count
    }
    
    func findSelectedPlant(plant: String) -> Int {
        var count = 0
        
        if (self.plantArray.count > 0) {
            
            for i in 0 ..< self.plantArray.count  {
                if (plant == self.plantArray[i] as! String) {
                    break
                }
                count += 1
            }
            
            
        }
        
        
        return count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if ( Shared.getData != nil ) {
            return Shared.getData.data.count
        }
        return 0
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if ( Shared.getData != nil && self.listPlant != nil ) {
        if ( Shared.getData != nil  ) {
        
//            print("numberOfRowsInSection \(section)")
            
            let sensorHeaderNum = 1
            let totalRows = totalBodyAndIos(section)
            let sensorBodyNum = totalRows.0
            let sensorControlNum = totalRows.1
            let total = sensorHeaderNum + sensorBodyNum + sensorControlNum
            print("numberOfRowsInSection \(section) total \(total)")
            return total
            
        }
        return 0
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if ( Shared.getData != nil   ) {
            
            if ( indexPath.row == 0 ) {
                return CGFloat(HEADER_HEIGHT)
            } else {
                let bodyNums = totalBodyAndIos(indexPath.section).0

                
                if ( indexPath.row >= 1 && indexPath.row <= (bodyNums+1) ) {
                    return CGFloat(BODY_HEIGHT )
                } else {
                    return CGFloat(IO_CELL_HEIGHT )
                }
            }
            
            
        }
        return 200

    }
    

     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        print("willDisplay tag(\(tableView.tag)) indexPath.section(\(indexPath.section)) indexPath.row(\(indexPath.row)))")
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("cellForRowAt tag(\(tableView.tag)) indexPath.section(\(indexPath.section)) indexPath.row(\(indexPath.row)))")
    
        var cell = UITableViewCell()
        
        if ( Shared.getData != nil ) {
            let index = indexPath.section
            let tmpData = Shared.getData.data[index]
            
            if ( indexPath.row == 0) {
                let headerCell = tableView.dequeueReusableCell(withIdentifier: "sensorHeaderCell", for: indexPath) as! SensorHeaderTableViewCell
                
                headerCell.lblSerial.text = NSLocalizedString("TRANSMITTERS", comment: "") + (tmpData.dispName.count > 0 ? tmpData.dispName : tmpData.sn )
                headerCell.imgPlant.isHidden = true
                if (tmpData.plant != nil ) {
                    headerCell.lblSerial.text! += "\n" + (tmpData.plant?.type)!
                    headerCell.imgPlant.image = UIImage(named: Shared.getPlantImageName(tmpData.plant!.type))
                    headerCell.imgPlant.isHidden = false
                }
                headerCell.lblSerial.numberOfLines = 0
                headerCell.lblSerial.sizeToFit()
                headerCell.lblSerial.setNeedsDisplay()
                
                
//                headerCell.lblDateTime.text = tmpData.data.datetime
                headerCell.butSetting.tag = index
                headerCell.butSetting.addTarget(self, action: #selector(showSetting), for: .touchUpInside)
                headerCell.butSetting.isHidden = true   // Hidden button setting, Use in future
                
                // Signal
                if ( tmpData.sgn == "1") {
                    headerCell.imgSignal.image = UIImage(named: "wireless_0")
                } else {
                    headerCell.imgSignal.image = UIImage(named: "wireless_4")
                }
                
                // Battery
                var batt = 0
                batt = Int( tmpData.batt )!
                if ( batt == 100  ) {
                    headerCell.imgBattery.image = UIImage(named: "bat_4")
                } else if (batt >= 60  ){
                    headerCell.imgBattery.image = UIImage(named: "bat_3")
                } else if (batt >= 30  ){
                    headerCell.imgBattery.image = UIImage(named: "bat_2")
                } else if (batt >= 5  ){
                    headerCell.imgBattery.image = UIImage(named: "bat_1")
                } else {
                    headerCell.imgBattery.image = UIImage(named: "bat_0")
                }
                
                cell = headerCell
                
                
            } else {
                
                let bodyNums = totalBodyAndIos(index).0
//                let ioNums = totalBodyAndIos(index).1
                
                if ( indexPath.row >= 1 && indexPath.row <= bodyNums ) {
                    let bodyCell = tableView.dequeueReusableCell(withIdentifier: "sensorBodyCell", for: indexPath) as! SensorBodyTableViewCell
                    
                    let getBodySensorValue: SENSOR_BODY = getBodySensor(index, dataIndex: indexPath.row-1)
                    
                    bodyCell.imgBackground.layer.cornerRadius = 8.0
                    bodyCell.imgBackground.clipsToBounds = true
                    
                    bodyCell.lblDataType.attributedText = getBodySensorValue.dataTypeAttibute
                    

                    bodyCell.lblIndicator.isHidden = true
                    
                    
                    
                    let dataValue = NSMutableAttributedString.init(string: getBodySensorValue.dataValue! + " " + getBodySensorValue.dataUnit!)
                    let fontSizeBig = CGFloat(30.0)
                    
                    bodyCell.lblDataValue.text = getBodySensorValue.dataValue! + " " + getBodySensorValue.dataUnit!
                    
                    if ( getBodySensorValue.indicatorValue! < 0 ) {
                        

                        
                        // set the custom font and color for the 0,1 range in string
                        dataValue.setAttributes([NSAttributedString.Key.font: UIFont(name: "DTAC 2017 Regular", size: fontSizeBig)!,
                                                 NSAttributedString.Key.foregroundColor: UIColor.dtacBlue],
                                            range: NSMakeRange(0, getBodySensorValue.dataValue!.unicodeScalars.count))
                        dataValue.setAttributes([NSAttributedString.Key.font: UIFont(name: "DTAC 2017 Regular", size: 14.0)!,
                                                 NSAttributedString.Key.foregroundColor: UIColor.dtacBlue],
                                            range: NSMakeRange(getBodySensorValue.dataValue!.unicodeScalars.count , getBodySensorValue.dataUnit!.unicodeScalars.count + 1))
                        bodyCell.lblDataValue.attributedText = dataValue
                        
                    } else if ( getBodySensorValue.indicatorValue! > 0 ) {
                       
                        
                        // set the custom font and color for the 0,1 range in string
                        dataValue.setAttributes([NSAttributedString.Key.font: UIFont(name: "DTAC 2017 Regular", size: fontSizeBig)!,
                                                 NSAttributedString.Key.foregroundColor: UIColor.dtacRed],
                                            range: NSMakeRange(0, getBodySensorValue.dataValue!.unicodeScalars.count))
                        dataValue.setAttributes([NSAttributedString.Key.font: UIFont(name: "DTAC 2017 Regular", size: 14.0)!,
                                                 NSAttributedString.Key.foregroundColor: UIColor.dtacRed],
                                            range: NSMakeRange(getBodySensorValue.dataValue!.unicodeScalars.count , getBodySensorValue.dataUnit!.unicodeScalars.count + 1))
                        bodyCell.lblDataValue.attributedText = dataValue
                        
                    } else {
//                        bodyCell.lblDataValue.attributedText = NSAttributedString(string: bodyCell.lblDataValue.text!, attributes: [NSFontAttributeName:UIFont(name: "DTAC 2017 Regular", size: 28.0)!,NSForegroundColorAttributeName:UIColor.dtacGreen])
                        
                        // set the custom font and color for the 0,1 range in string
                        dataValue.setAttributes([NSAttributedString.Key.font: UIFont(name: "DTAC 2017 Regular", size: fontSizeBig)!,
                                                 NSAttributedString.Key.foregroundColor: UIColor.dtacGreen],
                                            range: NSMakeRange(0, getBodySensorValue.dataValue!.unicodeScalars.count))
                        dataValue.setAttributes([NSAttributedString.Key.font: UIFont(name: "DTAC 2017 Regular", size: 14.0)!,
                                                 NSAttributedString.Key.foregroundColor: UIColor.dtacGreen],
                                            range: NSMakeRange(getBodySensorValue.dataValue!.unicodeScalars.count , getBodySensorValue.dataUnit!.unicodeScalars.count + 1))
                        bodyCell.lblDataValue.attributedText = dataValue
                    }
                    
                    bodyCell.lblDataValue.sizeToFit()
                    
//                    bodyCell.lblDataValue.isHidden = true
                    
//                    bodyCell.lblDataUnit.text = getBodySensorValue.dataUnit
//                    bodyCell.lblDataUnit.isHidden = true
                    
                    bodyCell.imgSensor.image = UIImage(named: getSensorImageName(getBodySensorValue.dataType!))
                    bodyCell.imgSensorIndicator.image =  UIImage(named: getSensorImageIndicator(getBodySensorValue.indicatorValue!)) 
                    
//                    bodyCell.imgSensor.image = UIImage(named: getSensorImageIndicator(getBodySensorValue.indicatorValue))
                    
                    
                    bodyCell.lblMin.text = NSLocalizedString("MIN", comment: "") + getBodySensorValue.minValue!
                    bodyCell.lblMax.text = NSLocalizedString("MAX", comment: "") + getBodySensorValue.maxValue!
                    
//                    bodyCell.lblDate.text = NSLocalizedString("DATE", comment: "")
//                    bodyCell.lblTime.text = NSLocalizedString("TIME", comment: "")
                    
//                    bodyCell.lblDateValue.text = getBodySensorValue.date
//                    bodyCell.lblTimeValue.text = getBodySensorValue.time
                    
                    
                    
                    cell = bodyCell
                } else {
                    let bodyNums = totalBodyAndIos(index).0
                    
                    let controlCell = tableView.dequeueReusableCell(withIdentifier: "sensorControlCell", for: indexPath) as! SensorControlTableViewCell
                    
                    
                    let tmpData = Shared.getData.data[index]
                    let tempIo = tmpData.io![indexPath.row - 1 - bodyNums]
                    
                    controlCell.imgBg.layer.cornerRadius = 8.0
                    controlCell.imgBg.clipsToBounds = true
                    
                    var uiImageControl: UIImage?
                    var ioName: String = ""
                    switch tempIo.type {
                    case "วาล์วสเปร์หมอก":
                        uiImageControl = UIImage(named: "valve_io_spray")
                        ioName = NSLocalizedString("IO_FOG_SPRAY", comment: "")
                        break
                    case "วาล์วน้ำหยด":
                        uiImageControl = UIImage(named: "valve_io_water_drop")
                        ioName = NSLocalizedString("IO_DROP_WATER", comment: "")
                        break
                    case "วาล์วปุ๋ยน้ำ":
                        uiImageControl = UIImage(named: "valve_io_fertilizer")
                        ioName = NSLocalizedString("IO_FERTILIZER", comment: "")
                        break
                    case "วาล์วก๊าซ CO2":
                        uiImageControl = UIImage(named: "valve_io_co2")
                        ioName = NSLocalizedString("IO_CO2", comment: "")
                        break
                    case "สวิตซ์ตัวให้ความร้อน":
                        uiImageControl = UIImage(named: "LightBuld-Red")
                        ioName = NSLocalizedString("IO_HEATER", comment: "")
                        break
                    default:
                        break
                    }
                    controlCell.imgControl.image = uiImageControl
                    
                    
                    
                    var uiImageSwtich: UIImage?
                    var strStatus: String = ""
                    var statusColor = UIColor.clear
                    switch tempIo.status {
                    case "0":
                        uiImageSwtich = UIImage(named: "switch_off")
                        strStatus = NSLocalizedString("SWITCH_OFF", comment: "")
                        statusColor = UIColor.dtacRed
                        break
                    case "1":
                        uiImageSwtich = UIImage(named: "switch_on")
                        strStatus = NSLocalizedString("SWITCH_ON", comment: "")
                        statusColor = UIColor.dtacGreen
                        break
                    default:
                        uiImageSwtich = nil
                        break
                    }
                    let strChannel: String = " [" + tempIo.channel + "] "
                    
                    let dataValue = NSMutableAttributedString.init(string: ioName + strChannel + strStatus)
                    
                    
                    dataValue.setAttributes([NSAttributedString.Key.font: UIFont(name: "DTAC 2017 Regular", size: 14.0)!,
                                             NSAttributedString.Key.foregroundColor: UIColor.dtacDarkGrey],
                                            range: NSMakeRange(0, ioName.unicodeScalars.count + strChannel.unicodeScalars.count))
                    dataValue.setAttributes([NSAttributedString.Key.font: UIFont(name: "DTAC 2017 Regular", size: 14.0)!,
                                             NSAttributedString.Key.foregroundColor: statusColor],
                                            range: NSMakeRange((ioName.unicodeScalars.count + strChannel.unicodeScalars.count)  , strStatus.unicodeScalars.count ))
                    
                    controlCell.lblControlType.attributedText = dataValue
                        
                    
                    controlCell.imgSwitch.image = uiImageSwtich
                    let controlChannel = ((index+1) * 100) + Int(tempIo.channel)!
//                    controlCell.imgSwitch.tag = controlChannel + Int(tempIo.channel)!
                    controlCell.imgSwitch.tag = controlChannel
                    
                    if Int( tempIo.status )! >= 0 {
                        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchOnOff(tapGestureRecognizer:)))
                        controlCell.imgSwitch.isUserInteractionEnabled = true
                        controlCell.imgSwitch.addGestureRecognizer(tapGestureRecognizer)
                    }
                    
                        
                    cell = controlCell
                }
                
                
            }
        }
        
        
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        print("didSelectRowAt tag(\(tableView.tag)) section(\(indexPath.section) row(\(indexPath.row)))")
        print("did select row")
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 { return 0 }
        
        let height: CGFloat = 20
        
        return height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 20))
        headerView.backgroundColor = UIColor.clear
        
        return headerView
    }
    
    // ==========================================================================
    // =====================================
    // MARK: - USER DEFINED FUNCTION
    // =====================================
    // ==========================================================================
    
    
    @objc func update() {
        DispatchQueue.main.async {
            self.refreshPlant{ () -> () in
                self.refreshGetData()
            }
            
        }
    }
    
    
    func startTimer() {
        let intervalTimer = 60.0 * Float(SeedUtil.refreshInterval)!
       
        NewPlantViewControllerV2.stopTimer()
        
        print ("==============   start new timer task2 --> \(intervalTimer)\n\n")
        
        NewPlantViewControllerV2.timer = Timer.scheduledTimer(timeInterval: TimeInterval(intervalTimer), target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    
     static func stopTimer() {
        
        print("Stop NewPlantViewController2 timer Task")
        if ( self.timer != nil ) {
            self.timer?.invalidate()
            NewPlantViewControllerV2.timer = nil
        }
        
    }
    
    func refreshPlant(completed: @escaping FINISHED_REFRESH_PLANT) {
        
        print("refreshPlant")
//        SeedUtil.listPlant(token: SeedUtil.token, plant: "", completionHandler: {
//
//            result, message, obj in
//
//            if ( result == 0 ) {
//
//                self.listPlant = (obj as! LIST_PLANTS)
//
//
//
//                self.plantArray = self.listPlant.list as! NSArray
//                let selectedItem = self.findSelectedPlant(plant: self.listPlant.plant)
//
//                DispatchQueue.main.async {
//                    self.pickerPlant.reloadAllComponents()
//
//                    self.pickerPlant.selectRow(selectedItem, inComponent: 0, animated: false)
//
//                }
//
//
//
//                print("list array value is \(self.plantArray)")
//
//
//            } else {
//
//                print("error listplan \(message)")
//            }
//            completed()
//
//        })
        completed()
    }
    
    
    
    func refreshGetData() {
        
        print("refreshGetData")
        
        DispatchQueue.main.async {
            _ = Util.displayPendingAlert(uiViewController: self, message: "LOADING")
        }
        
        SeedUtil.getData(token: SeedUtil.token, completionHandler: {
            
            result, message, obj in
            
            if ( result == 0 ) {
                
                Shared.getData = (obj as! GET_DATA)
                
                
                
                // print("get data value is \(getData)")
                print("total sensors is \(Shared.getData.data.count)")
                DispatchQueue.main.async {
                    
                    if Shared.getData != nil && Shared.getData.data.count > 0 {
                        self.lblDate.text = NSLocalizedString("DATE", comment: "") + Shared.getData.data[0].data.datetime.prefix(10)
                        self.lblDate.numberOfLines = 0
                        self.lblDate.sizeToFit()
                        self.lblTime.text = NSLocalizedString("TIME", comment: "") + Shared.getData.data[0].data.datetime.suffix(8)
                        self.lblTime.numberOfLines = 0
                        self.lblTime.sizeToFit()
                    }
                    
                    self.tblSensor.reloadData()
                    Util.animateMyViews(viewToHide: self.tblSensor, viewToShow: self.tblSensor)
                    
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                }
                
                
                
                
//                for i in 0 ..< Shared.getData.data.count  {
//                    print(" batt \(Shared.getData.data[i].batt) btn: \(Shared.getData.data[i].btn)")
//                    print("data1: \(Shared.getData.data[i].data.data1) type1: \(Shared.getData.data[i].data.type1) unit1: \(Shared.getData.data[i].data.unit1)")
//                    print("data2: \(Shared.getData.data[i].data.data2) type2: \(Shared.getData.data[i].data.type2) unit2: \(Shared.getData.data[i].data.unit2)")
//
//                }
                
            } else {
                
                print("error getdata \(message)")
                
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
            
            
            
            
        })
    }
    
    // ====================================================== //
    // ===================== UI TAB BAR ===================== //
    // ====================================================== //
    
    //    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    //        print("item title =  \(item.tag)")
    //    }
    
    deinit {
        NewPlantViewControllerV2.timer?.invalidate()
    }
    
    @IBAction func logout(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("LOGOUT_TITLE", comment: ""), message: NSLocalizedString("LOGOUT_MESSAGE", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            
            SeedUtil.logout(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken,   completionHandler: {
                
                result, message in
                
                if ( result == 0 ) {
                    
                    NewPlantViewControllerV2.stopTimer()
                    
                    DispatchQueue.main.async {
                        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                        self.show(vc as! UIViewController, sender: vc)
                    }
                    
                } else {
                    
                    
                    DispatchQueue.main.async {
                        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                        self.show(vc as! UIViewController, sender: vc)
                    }
                    
                }
                
                
            })
            
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func totalBodyAndIos(_ index:Int) -> (Int, Int) {
        var numRows = 0
        var numIo = 0
        
        if ( Shared.getData != nil ) {
            
            let tmpData = Shared.getData.data[index]
            
            if tmpData.data.data1.count > 0 { numRows += 1 }
            if tmpData.data.data2.count > 0 { numRows += 1 }
            if tmpData.data.data3 != nil && tmpData.data.data3!.count > 0 { numRows += 1 }
            if tmpData.data.data4 != nil && tmpData.data.data4!.count > 0 { numRows += 1 }
            if tmpData.data.data5 != nil && tmpData.data.data5!.count > 0 { numRows += 1 }
            if tmpData.data.data6 != nil && tmpData.data.data6!.count > 0 { numRows += 1 }
            if tmpData.data.data7 != nil && tmpData.data.data7!.count > 0 { numRows += 1 }
            if tmpData.data.data8 != nil && tmpData.data.data8!.count > 0 { numRows += 1 }
            
            if tmpData.io != nil {
                numIo = (tmpData.io?.count)!
            }
        }
        
        
        
        return (numRows, numIo)
    }
    
    func getBodySensor(_ index: Int, dataIndex: Int) -> SENSOR_BODY {
        let tmpData = Shared.getData.data[index]
        
        var minHumidity = 0, maxHumidity = 0
        var minSoilHumidity = 0, maxSoilHumidity = 0
        var minTemperature = 0, maxTemperature = 0
        var minHeatTemperature = 0, maxHeatTemperature = 0
        var minLight = 0, maxLight = 0
        var minCO2 = 0, maxCO2 = 0
        
        if tmpData.plant != nil {
            minHumidity = Int(tmpData.plant!.hu_min)!
            maxHumidity = Int(tmpData.plant!.hu_max)!
            minSoilHumidity = Int(tmpData.plant!.soi_hu_min)!
            maxSoilHumidity = Int(tmpData.plant!.soi_hu_max)!
            minTemperature = Int(tmpData.plant!.temp_min)!
            maxTemperature = Int(tmpData.plant!.temp_max)!
            minLight = Int(tmpData.plant!.light_min)!
            maxLight = Int(tmpData.plant!.light_max)!
            minCO2 = Int(tmpData.plant!.co2_min!)!
            maxCO2 = Int(tmpData.plant!.co2_max!)!
        }
        
        
        
        let ret:SENSOR_BODY = SENSOR_BODY()
        
        var dataValue = ""
        var dataType = ""
        var dataUnit = ""
        let date = tmpData.data.datetime.prefix(10)
        let time = tmpData.data.datetime.suffix(8)
        
        // mapping data to array
        let listData = NSMutableArray()
        let listType = NSMutableArray()
        let listUnit = NSMutableArray()
        
        if (tmpData.data.data1.count > 0 ) {
            listData.add(tmpData.data.data1)
            listType.add(tmpData.data.type1)
            listUnit.add(tmpData.data.unit1)
        }
        if (tmpData.data.data2.count > 0 ) {
            listData.add(tmpData.data.data2)
            listType.add(tmpData.data.type2)
            listUnit.add(tmpData.data.unit2)
        }
        if (tmpData.data.data3 != nil && tmpData.data.data3!.count > 0 ) {
            listData.add(tmpData.data.data3!)
            listType.add(tmpData.data.type3!)
            listUnit.add(tmpData.data.unit3!)
        }
        if (tmpData.data.data4 != nil && tmpData.data.data4!.count > 0 ) {
            listData.add(tmpData.data.data4!)
            listType.add(tmpData.data.type4!)
            listUnit.add(tmpData.data.unit4!)
        }
        if (tmpData.data.data5 != nil && tmpData.data.data5!.count > 0 ) {
            listData.add(tmpData.data.data5!)
            listType.add(tmpData.data.type5!)
            listUnit.add(tmpData.data.unit5!)
        }
        if (tmpData.data.data6 != nil && tmpData.data.data6!.count > 0 ) {
            listData.add(tmpData.data.data6!)
            listType.add(tmpData.data.type6!)
            listUnit.add(tmpData.data.unit6!)
        }
        if (tmpData.data.data7 != nil && tmpData.data.data7!.count > 0 ) {
            listData.add(tmpData.data.data7!)
            listType.add(tmpData.data.type7!)
            listUnit.add(tmpData.data.unit7!)
        }
        if (tmpData.data.data8 != nil && tmpData.data.data8!.count > 0 ) {
            listData.add(tmpData.data.data8!)
            listType.add(tmpData.data.type8!)
            listUnit.add(tmpData.data.unit8!)
        }
        // mapping data to array
        
        
        dataType = listType.object(at: dataIndex) as! String
        dataValue = listData.object(at: dataIndex) as! String
        dataUnit = listUnit.object(at: dataIndex) as! String
        
        ret.dataType = dataType
        ret.dataValue = dataValue
        ret.dataUnit = dataUnit
        
        dataValue = dataValue.replacingOccurrences(of: ",", with: "")
        
        var indicatorAttibute: NSAttributedString? = nil
        var indicatorMsg: String? = nil
        var indicatorValue: Int = 0
        var minValue: String?
        var maxValue: String?
        
        var strPartA = ""
        let fontDataTypeSize = [CGFloat(12.0), CGFloat(13.0)]
        let fontDataTypeIndiSize = [CGFloat(12.0), CGFloat(13.0)]
        let mutableAttributedString = NSMutableAttributedString()
        var dataTypeAAttribute = [
            NSAttributedString.Key.font: UIFont(name: "DTAC 2017 Regular", size: fontDataTypeSize[0])!,
            NSAttributedString.Key.foregroundColor: UIColor(white: 77.0 / 255.0, alpha: 1.0)
        ]
        var dataTypeBAttribute = [
            NSAttributedString.Key.font: UIFont(name: "DTAC 2017 Regular", size: fontDataTypeIndiSize[0])!,
            NSAttributedString.Key.foregroundColor: UIColor.dtacBlue
        ]
        if ( screenWidth <= 320 ) {
        } else {
            dataTypeAAttribute.updateValue(UIFont(name: "DTAC 2017 Regular", size: fontDataTypeSize[1])!, forKey: NSAttributedString.Key.font)
            dataTypeBAttribute.updateValue(UIFont(name: "DTAC 2017 Regular", size: fontDataTypeSize[1])!, forKey: NSAttributedString.Key.font)
        }
        
        
        switch dataType {
        case "ความชื้น":
            let tmp = getSensorStatus(val: Float(dataValue)!, min: minHumidity, max: maxHumidity)
            indicatorMsg = tmp.0
            indicatorAttibute = (tmp.1 as! NSAttributedString)
            indicatorValue = tmp.2
            minValue = String(minHumidity)
            maxValue = String(maxHumidity)
            
            
            if ( screenWidth <= 320 ) {
                strPartA = NSLocalizedString("AIR_HUMIDITY", comment: "") + "\n"
            } else {
                strPartA = NSLocalizedString("AIR_HUMIDITY", comment: "") + " "
            }
            
            
            break
        case "ความชื้นดิน":
            let tmp = getSensorStatus(val: Float(dataValue)!, min: minSoilHumidity, max: maxSoilHumidity)
            indicatorMsg = tmp.0
            indicatorAttibute = (tmp.1 as! NSAttributedString)
            indicatorValue = tmp.2
            minValue = String(minSoilHumidity)
            maxValue = String(maxSoilHumidity)
            
            if ( screenWidth <= 320 ) {
                strPartA = NSLocalizedString("SOIL_HUMIDITY", comment: "") + "\n"
            } else {
                strPartA = NSLocalizedString("SOIL_HUMIDITY", comment: "") + " "
            }
            
            
            break
        case "CO2":
            let tmp = getSensorStatus(val: Float(dataValue)!, min: minCO2, max: maxCO2)
            indicatorMsg = tmp.0
            indicatorAttibute = (tmp.1 as! NSAttributedString)
            indicatorValue = tmp.2
            minValue = String(minCO2)
            maxValue = String(maxCO2)
            
            if ( screenWidth <= 320 ) {
                strPartA = NSLocalizedString("CO2", comment: "") + "\n"
            } else {
                strPartA = NSLocalizedString("CO2", comment: "") + " "
            }
            
            
            break
        case "อุณหภูมิ":
            let tmp = getSensorStatus(val: Float(dataValue)!, min: minTemperature, max: maxTemperature)
            indicatorMsg = tmp.0
            indicatorAttibute = (tmp.1 as! NSAttributedString)
            indicatorValue = tmp.2
            minValue = String(minTemperature)
            maxValue = String(maxTemperature)
            
            if ( screenWidth <= 320 ) {
                strPartA = NSLocalizedString("TEMPERATURE", comment: "") + "\n"
            } else {
                strPartA = NSLocalizedString("TEMPERATURE", comment: "") + " "
            }
            
            break
        case "แสง":
            let tmp = getSensorStatus(val: Float(dataValue)!, min: minLight, max: maxLight)
            indicatorMsg = tmp.0
            indicatorAttibute = (tmp.1 as! NSAttributedString)
            indicatorValue = tmp.2
            minValue = String(minLight)
            maxValue = String(maxLight)
            
            if ( screenWidth <= 320 ) {
                strPartA = NSLocalizedString("LIGHT", comment: "") + "\n"
            } else {
                strPartA = NSLocalizedString("LIGHT", comment: "") + " "
            }
            
            
            break
        case "ความร้อน":
            let tmp = getSensorStatus(val: Float(dataValue)!, min: minHeatTemperature, max: maxHeatTemperature)
            indicatorMsg = tmp.0
            indicatorAttibute = (tmp.1 as! NSAttributedString)
            indicatorValue = tmp.2
            minValue = String(minHeatTemperature)
            maxValue = String(maxHeatTemperature)
            
            if ( screenWidth <= 320 ) {
                strPartA = NSLocalizedString("TEMPERATURE", comment: "") + "\n"
            } else {
                strPartA = NSLocalizedString("TEMPERATURE", comment: "") + " "
            }
            
            break
        default:
            let tmp = getSensorStatus(val: Float(dataValue)!, min: minSoilHumidity, max: maxSoilHumidity)
            indicatorMsg = tmp.0
            indicatorAttibute = (tmp.1 as! NSAttributedString)
            indicatorValue = tmp.2
            minValue = String(minSoilHumidity)
            maxValue = String(maxSoilHumidity)
            
            if ( screenWidth <= 320 ) {
                strPartA = NSLocalizedString("SOIL_HUMIDITY", comment: "") + "\n"
            } else {
                strPartA = NSLocalizedString("SOIL_HUMIDITY", comment: "") + " "
            }
            
            
            break
        }
        
        switch indicatorValue {
        case -1:
            dataTypeBAttribute.updateValue(UIColor.dtacBlue, forKey: NSAttributedString.Key.foregroundColor)
            
            break
        case 0:
            dataTypeBAttribute.updateValue(UIColor.dtacGreen, forKey: NSAttributedString.Key.foregroundColor)
            
            break
        case 1:
            dataTypeBAttribute.updateValue(UIColor.dtacRed, forKey: NSAttributedString.Key.foregroundColor)
            
            break
        default:
            break
        }
        
        let dataTypeNameA = NSAttributedString(string: strPartA , attributes: dataTypeAAttribute)
        let dataTypeNameB = NSAttributedString(string: indicatorMsg!, attributes: dataTypeBAttribute)
        mutableAttributedString.append(dataTypeNameA)
        mutableAttributedString.append(dataTypeNameB)
        
        ret.dataTypeAttibute = mutableAttributedString
        
        ret.indicatorAttibute = indicatorAttibute!
        ret.indicatorMsg = indicatorMsg!
        ret.indicatorValue = indicatorValue
        ret.date = String(date)
        ret.time = String(time)
        ret.minValue = minValue
        ret.maxValue = maxValue
        
        return ret
    }
    
    func getSensorStatus(val: Float, min: Int, max: Int) -> (String, Any, Int) {
        var indicatorValue: String? = nil
        var indicatorAttibute: NSAttributedString? = nil
        var minMaxNormalValue: Int = 0
        if ( val > Float(max)) {
            indicatorValue = "[" + NSLocalizedString("HIGHER_MAX_VALUE", comment: "") + "]"
            indicatorAttibute = NSAttributedString(string: indicatorValue! , attributes: [NSAttributedString.Key.font:UIFont(name: "DTAC 2017 Regular", size: 10.0)!,NSAttributedString.Key.foregroundColor: UIColor.dtacRed])
            minMaxNormalValue = 1
        } else if ( val < Float(min)) {
            indicatorValue = "[" + NSLocalizedString("LOWER_MIN_VALUE", comment: "") + "]"
            indicatorAttibute = NSAttributedString(string: indicatorValue! , attributes: [NSAttributedString.Key.font:UIFont(name: "DTAC 2017 Regular", size: 10.0)!,NSAttributedString.Key.foregroundColor: UIColor.dtacBlue])
            minMaxNormalValue = -1
        } else {
            indicatorValue = "[" + NSLocalizedString("NORMAL_VALUE", comment: "") + "]"
            indicatorAttibute = NSAttributedString(string: indicatorValue! , attributes: [NSAttributedString.Key.font:UIFont(name: "DTAC 2017 Regular", size: 10.0)!,NSAttributedString.Key.foregroundColor: UIColor.dtacGreen])
            minMaxNormalValue = 0
        }
        
        return (indicatorValue!, indicatorAttibute!, minMaxNormalValue)
    }
    
    func getSensorImageName(_ unitType: String) -> String {
        
        var imageName = ""
        switch (unitType) {
        case "อุณหภูมิ":
            imageName = "temperature"
            break
        case "แสง":
            imageName = "light"
            break
        case "ความชื้น":
            imageName = "air_humidity"
            break
        case "ความชื้นดิน":
            imageName = "soil_humidity"
            break
        case "CO2":
            imageName = "co2"
            break
        default:
            imageName = ""
        }
        
        return imageName
        
    }
    
    func getSensorImageIndicator(_ val: Int) -> String {
        
        var imageName = ""
        switch (val) {
        case -1:
            imageName = "blue_circle"
            break
        case 0:
            imageName = "green_circle"
            break
        case 1:
            imageName = "red_circle"
            break
        default:
            imageName = ""
        }
        
        return imageName
        
    }
    
    @objc func showSetting(_ sender: UIButton) {
        let tag = sender.tag
        
        print("showImage click tag \(tag)")
        
//        if ( Shared.getData.image![tag].image_url.count > 0 ) {
//            print("showImage url \(Shared.getData.image![tag].image_url)")
//        }
        

        
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.update()
        refreshControl.endRefreshing()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "houseSegue") {
            houseTableViewController = segue.destination as? HouseTableViewController
            houseTableViewController?.view.backgroundColor = UIColor.clear
        }
    }
    
    
    @objc func showSelectHouse(_ sender: UITapGestureRecognizer) {
        print("New Plant arrow click")
        
        
        houseConstraintHeight.constant = screenHeight
    }
    
    @objc func switchOnOff(tapGestureRecognizer: UITapGestureRecognizer) {
        
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        var index = roundToHundreds(tappedImage.tag)
        index = (index / 100) - 1
        let channel = tappedImage.tag % 100
        
        let tmpGetData = Shared.getData.data[index]
        var status: String?
        
        for control in tmpGetData.io! {
            if control.channel == String(channel) {
                if ( control.status == "0" ) {
                    status = "1"
                } else {
                    status = "0"
                }
            }
        }
        
        
        // SeedUtil call control
        let alert = Util.displayPendingAlert(uiViewController: self, message: "LOADING")
        SeedUtil.control(token: SeedUtil.token, sn: tmpGetData.sn, channel: String.init(channel) , message: status!, completionHandler: {
            result, message, object in
            
            DispatchQueue.main.async {
                alert.dismiss(animated: true, completion: nil)
                
            }
            
            if ( result == 0) {
                
                self.update()
                
                
                
            } else {
                
                print("control error \(message)")
                
                
                
            }
        })
        
        
        
    }
    
    func roundToHundreds(_ value: Int) -> Int {
        return value/100 * 100 + (value % 100)/50 * 100
    }
    
}

