//
//  SettingsViewController.swift
//  argiculture
//
//  Created by Thanachai on 5/31/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import UIKit

class SettingsViewController: MainTemplateViewController, UITextFieldDelegate {


    
    @IBOutlet weak var bgImgView: UIImageView!
    @IBOutlet weak var switchEnable: UISwitch!
    @IBOutlet weak var butSave: UIView!
    
    @IBOutlet weak var lblStartTime: UILabel!
    
    @IBOutlet weak var txtStartTime: UITextField!
    @IBOutlet weak var lblStopTime: UILabel!
    
    @IBOutlet weak var txtStopTime: UITextField!
    
    var settings: SETTINGS!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bgImgView.addBlurEffect()
        
        txtStartTime.delegate = self
        txtStopTime.delegate = self
        
//        switchEnable.setOn((SeedUtil.enableNotification == "1" ? true : false)  , animated: true)
        
        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
    }

    override func didReceiveMemoryWarning() {
               super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SeedUtil.saveSettings(token: SeedUtil.token, enableNoti: "", startTime: "", endTime: "", fcm: "", completionHandler: {
            
            result, message, obj in
            
            if ( result == 0 ) {
                
                self.settings = (obj as! SETTINGS)
                
                if ( self.settings.errorCode == "0") {
                    
                    
                    DispatchQueue.main.async {
                        
                        if (self.settings.enablenoti.count > 0 ) {
                            
                            if ( self.settings.enablenoti == "0") {
                                self.switchEnable.setOn(false, animated: true)
                            } else {
                                self.switchEnable.setOn(true, animated: true)
                            }
                        }
                        
                        
                        if (self.settings.starttime.count > 0 ) {
                            self.txtStartTime.text = self.settings.starttime
                        }
                        
                        if (self.settings.endtime.count > 0 ) {
                            self.txtStopTime.text = self.settings.endtime
                        }
                        
                        self.onOffSetting(self.switchEnable)
                    }
                    
                }
                
            } else {
                
                // alert message
                Util.showMessage(view: self, title: NSLocalizedString("ERROR_TITLE", comment: ""), message: NSLocalizedString(message, comment: "") , alertTitle: NSLocalizedString("ERROR_TITLE", comment: ""))
            }
            
        })
        
        
       
    }
    
    @IBAction func saveSettings(_ sender: Any) {
        
        
        if ( txtStartTime.text?.count == 0 || txtStopTime.text?.count == 0) {
            Util.showMessage(view: self, title: NSLocalizedString("ERROR_TITLE", comment: ""), message: NSLocalizedString("MISSING_START_STOP_TIME", comment: "") , alertTitle: NSLocalizedString("OK", comment: ""))
        } else if ( Int(txtStartTime.text!)! == Int(txtStopTime.text!)! ) {
            
            Util.showMessage(view: self, title: NSLocalizedString("ERROR_TITLE", comment: ""), message: NSLocalizedString("STARTTIME_EQUAL_STOPTIME", comment: "") , alertTitle: NSLocalizedString("OK", comment: ""))
            
        } else {
            
            var enableNoti = "0"
            let startTime = txtStartTime.text!
            let stopTime = txtStopTime.text!
            if (switchEnable.isOn) {
                enableNoti = "1"
                
            }
            
            SeedUtil.saveSettings(token: SeedUtil.token, enableNoti: enableNoti, startTime: startTime, endTime: stopTime, fcm: SeedUtil.tokenFcm, completionHandler: { result, message, obj in
                
                if (result == 0) {
                    print("save settings success")
                    
                    DispatchQueue.main.async {
                        SeedUtil.startTime = self.txtStartTime.text!
                        SeedUtil.stopTime = self.txtStopTime.text!
                        
                        Util.showMessage(view: self, title: NSLocalizedString("SUCCEED", comment: ""), message: NSLocalizedString("SAVE_SETTINGS_SUCCEED", comment: "") , alertTitle: NSLocalizedString("OK", comment: ""))
                    }
                    
                    
                    
                    
                } else if (result > 1 || result < 0) {
                    
                    DispatchQueue.main.async {
                        Util.showMessage(view: self, title: NSLocalizedString("ERROR_TITLE", comment: ""), message: NSLocalizedString(message, comment: "") , alertTitle: NSLocalizedString("OK", comment: ""))
                    }
                    
                    
                }
                
            })
            
        }
        
        
    }
    
    @IBAction func onOffSetting(_ sender: Any) {
        
        if (switchEnable.isOn) {
            lblStopTime.alpha = 1.0
            lblStartTime.alpha = 1.0
            txtStartTime.isUserInteractionEnabled = true
            txtStopTime.isUserInteractionEnabled = true
            txtStartTime.alpha = 1.0
            txtStopTime.alpha = 1.0
        } else {
            lblStopTime.alpha = 0.5
            lblStartTime.alpha = 0.5
            txtStartTime.isUserInteractionEnabled = false
            txtStopTime.isUserInteractionEnabled = false
            txtStartTime.alpha = 0.5
            txtStopTime.alpha = 0.5
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        
        let result = string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
        
        if ( (textField.text?.count)! + string.count > 2) {
            textField.text = "23"
//            SeedUtil.refreshInterval = "99"
            return false
        } else if ( ((textField.text?.count)! + string.count == 2 ) && Int(textField.text!)! > 23) {
            textField.text = "23"
            //            SeedUtil.refreshInterval = "99"
            return false
        } 
        
        return result
    }
  
}
