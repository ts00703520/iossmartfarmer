//
//  MenuHeaderViewController.swift
//  argiculture
//
//  Created by thanachais on 18/3/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit
import SideMenu
import Firebase

class MenuHeaderViewController: UIViewController {

    @IBOutlet weak var lblUser: UILabel!
    
    
    @IBOutlet weak var butMenu: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        let root : UIViewController! = self.storyboard!.instantiateViewController(withIdentifier: "MenuTableViewController")
//        let main : UIViewController! = self.storyboard!.instantiateViewController(withIdentifier: "NewPlantViewControllerV2")
        
        // Define the menus
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: root)
        // UISideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
        // let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
        SideMenuManager.default.leftMenuNavigationController = menuLeftNavigationController
        
        
        
        // (Optional) Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
        
//        SideMenuManager.default.menuAddPanGestureToPresent(toView: main.navigationController!.navigationBar)
//        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: main.navigationController!.view)
        
        // (Optional) Prevent status bar area from turning black when menu appears:
        SideMenuManager.default.menuFadeStatusBar = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblUser.text = SeedUtil.userLogin
        
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    @IBAction func logout(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("LOGOUT_TITLE", comment: ""), message: NSLocalizedString("LOGOUT_MESSAGE", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            
            Util.displayPendingAlert(uiViewController: self, message: "LOADING")
            SeedUtil.logout(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken,   completionHandler: {
                
                result, message in
                
                DispatchQueue.main.async {
                                        
                    self.dismiss(animated: true, completion: {
                        
                        if ( result == 0 ) {
                                            
                                            NewPlantViewControllerV2.stopTimer()
                                            
                                            DispatchQueue.main.async {
                                                
                        //                        self.dismiss(animated: true, completion: {
                        //
                        //
                        //
                        //                        })
                                                
                                                let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                                                self.show(vc as! UIViewController, sender: vc)
                                                
                                                
                                            }
                                            
                                        } else {
                                            
                                            
                                            DispatchQueue.main.async {
                                                
                        //                        self.dismiss(animated: true, completion: {
                        //
                        //
                        //
                        //                        })
                                                
                                                let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                                                self.show(vc as! UIViewController, sender: vc)
                                                
                                                
                                                
                                                
                                            }
                                            
                                        }
                        
                    })
                }
                
                
            })
            
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
