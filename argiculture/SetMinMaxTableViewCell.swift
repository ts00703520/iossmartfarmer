//
//  SetMinMaxTableViewCell.swift
//  argiculture
//
//  Created by thanachais on 10/5/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class SetMinMaxTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewBody: UIView!
    
    @IBOutlet weak var imgPlant: UIImageView!
    
    @IBOutlet weak var lblMin: UILabel!
    @IBOutlet weak var lblMax: UILabel!
    
    @IBOutlet weak var txtMin: UITextField!
    @IBOutlet weak var txtMax: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
