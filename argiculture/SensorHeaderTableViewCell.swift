//
//  SensorHeaderTableViewCell.swift
//  argiculture
//
//  Created by thanachais on 5/3/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//


import Foundation
import UIKit

class SensorHeaderTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("SensorHeaderTableViewCell indexPath.row \(indexPath.row)")
        return UITableViewCell()
    }
    
    
    var parentRow = -999
    
    @IBOutlet weak var lblSerial: UILabel!
    
    @IBOutlet weak var lblDateTime: UILabel!
    

    @IBOutlet weak var imgBattery: UIImageView!
    @IBOutlet weak var imgSignal: UIImageView!
    
    @IBOutlet weak var butSetting: UIButton!
    
    @IBOutlet weak var imgPlant: UIImageView!
    
    var data: DATA? = nil
    
   
    
}


