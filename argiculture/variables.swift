//
//  authen.swift
//  argiculture
//
//  Created by Thanachai on 3/8/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import Foundation

var LOCALE: String?



enum Function {
    case authen, isValidToken, listPlant, getData, logout, settings, refreshFcm, listHouse, setPlant, version, control, plantSetting  // , notiHistory
}

struct AUTHEN {
    
    
    let errorCode: String
    let errorMessage: String
    let token: String
    let user: (String)
    
}

struct IS_VALID_TOKEN {
    
    
    let errorCode: String
    let errorMessage: String
}

struct LOGOUT {
    
    
    let errorCode: String
    let errorMessage: String
}

struct VERSION {
    
    
    let errorCode: String
    let errorMessage: String
    let version: String
}

struct LIST_PLANTS {
    
    let errorCode: String
    let errorMessage: String
    let plant: String
    let rate: String
    let range: (
        type: String,
        temp_min: String,
        temp_max: String,
        hu_min: String,
        hu_max: String,
        light_min: String,
        light_max: String,
        soi_hu_min: String,
        soi_hu_max: String,
        co2_min: String?,
        co2_max: String?
    )
    let list: AnyObject
    
}

struct LIST_HOUSE {
    
    let errorCode: String
    let errorMessage: String
    let house: [HOUSE]
    
}
struct HOUSE {
    let name: String
    let id: String
}

struct GET_DATA {
    
    let errorCode: String
    let errorMessage: String
    let data: [GET_DATA_SET]
    let image: [IMAGE]?
}

struct GET_DATA_SET {
    let sn: String
    let dispName: String
    let batt: String
    let btn: String
    let sgn: String
    let key: String?
    let secret: String?
    let plant: RANGE?
    
//    let data: (
//    datetime: String,
//    data1: String,
//    data2: String,
//    type1: String,
//    type2: String,
//    unit1: String,
//    unit2: String
//    )
    let data: DATA
    let io: [IO]?
}

struct RANGE {
    let type: String
    let temp_min: String
    let temp_max: String
    let hu_min: String
    let hu_max: String
    let light_min: String
    let light_max: String
    let soi_hu_min: String
    let soi_hu_max: String
    let co2_min: String?
    let co2_max: String?
}

struct RANGE_MODIFY {
    var type: String
    var temp_min: String
    var temp_max: String
    var hu_min: String
    var hu_max: String
    var light_min: String
    var light_max: String
    var soi_hu_min: String
    var soi_hu_max: String
    var co2_min: String?
    var co2_max: String?
}

struct DATA {
    let datetime: String
    let data1: String
    let data2: String
    let data3: String?
    let data4: String?
    let data5: String?
    let data6: String?
    let data7: String?
    let data8: String?
    let type1: String
    let type2: String
    let type3: String?
    let type4: String?
    let type5: String?
    let type6: String?
    let type7: String?
    let type8: String?
    let unit1: String
    let unit2: String
    let unit3: String?
    let unit4: String?
    let unit5: String?
    let unit6: String?
    let unit7: String?
    let unit8: String?
}

struct IO {
    
    let channel: String
    let status: String
    let sn: String
    let type: String

}

struct IMAGE {
    
    let image_url: String
    
}

struct SAVE_SETTINGS {
    
    
    let errorCode: String
    let errorMessage: String
}

struct SETTINGS {
    let errorCode: String
    let errorMessage: String
    let enablenoti: String
    let starttime: String
    let endtime: String
//    let fcm: String

}

struct REFRESH_FCM {
    
    
    let errorCode: String
    let errorMessage: String
}

struct NOTI_DATA {
    
    let errorCode: String
    let errorMessage: String
    let data: [NOTI_HISTORY_SET]
}

struct SET_PLANT {
    
    let errorCode: String
    let errorMessage: String
    let changed: String?
}

struct NOTI_HISTORY_SET {
    let datetime: String
    let msg: String
}


class SENSOR_BODY {
    
    var dataType: String?
    var dataValue: String?
    var dataUnit: String?
    var dataTypeAttibute: NSAttributedString?
    var indicatorAttibute: NSAttributedString?
    var indicatorMsg: String?
    var indicatorValue: Int?
    var date: String?
    var time: String?
    var minValue: String?
    var maxValue: String?
}

struct CONTROL {
    
    
    let errorCode: String
    let errorMessage: String
    let data: CONTROL_RESP?
}

struct PLANT_SETTING {
    
    
    let errorCode: String
    let errorMessage: String
    let list: [RANGE]?
}

struct CONTROL_RESP {
    var code: String?
    var message: String?
    var httpCode: String?
}
