//
//  ViewController.swift
//  argiculture
//
//  Created by Thanachai on 2/27/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import UIKit
import Firebase
// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

class MainViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var butLogin: UIButton!
    
    @IBOutlet weak var imgSplash: UIImageView!
    
//    let PLAN_VIEW_CONTROLLER_NAME = "NewPlantViewControllerV2"
    
    
    override func viewDidLoad() {
        
        // [START log_fcm_reg_token]
        let tokenFcm = Messaging.messaging().fcmToken
        
        print("FCM token: \(tokenFcm ?? "")")
        SeedUtil.tokenFcm = (tokenFcm ?? "")
        // [END log_fcm_reg_token]
        
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        txtUsername.delegate = self
        txtPassword.delegate = self
        
        let userImage = UIImageView(image: UIImage(named: "login_user"))
        userImage.frame = CGRect(x: 4, y: 3, width: 24, height: 24)
        
        let passImage = UIImageView(image: UIImage(named: "login_password"))
        passImage.frame = CGRect(x: 4, y: 3, width: 24, height: 24)
        
        userImage.contentMode = .scaleAspectFit
        passImage.contentMode = .scaleAspectFit
        
        txtUsername.leftView = userImage
        
        txtUsername.leftViewMode = UITextField.ViewMode.always
        
        txtPassword.leftView = passImage
        txtPassword.leftViewMode = UITextField.ViewMode.always
        
        
        
        
        disableControl(flag: true)
        
            // redirect to next page
        
        self.hideKeyboardWhenTappedAround() 
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        LOCALE = Util.getLocale()
        let token = SeedUtil.token
        
        checkVersion()
        checkToken(token: token, fcm: Messaging.messaging().fcmToken)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Shared.getData = nil
    }
    
    func disableControl(flag: Bool) {
        DispatchQueue.main.async(execute: {
        switch flag {
        case true:
            self.txtUsername.isUserInteractionEnabled = false
            self.txtPassword.isUserInteractionEnabled = false
            self.butLogin.isUserInteractionEnabled = false
            break
        default:
            self.txtUsername.isUserInteractionEnabled = true
            self.txtPassword.isUserInteractionEnabled = true
            self.butLogin.isUserInteractionEnabled = true
        }
        })
    }
    
    func checkVersion() {
        SeedUtil.checkVersion(os: "ios", completionHandler: {result, message, object in
            
            if result == 0 {
                
                let ret = object as! VERSION
                
                if ( Shared.version < Double( ret.version)! ) {
                    
                    let alert = UIAlertController(title: NSLocalizedString("VERSION", comment: ""), message: NSLocalizedString("UPDATE_VERSION", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                        
                        DispatchQueue.main.async {
                            UIApplication.shared.openURL(URL(string: Shared.appleStoreLink)!)
                        }
                        
                    }))
                    DispatchQueue.main.async {
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                
            } else {
                print("error message \(message)")
            }
            
            
        })
    }
    
    func checkToken(token: String, fcm: String?) {
        
        if ( Util.str(token).count > 0) {
            
            let url = "https://magicporting.dtac.co.th:12011/seedbox/isValidToken/"
            var data = "token=" + token
            
            if ( Util.str(fcm).count > 0) {
                data += "&fcm=" + fcm!
            }
            
            HTTPUtil.async(url: url, method: HTTPUtil.METHOD.POST, secTimeout: 10, data: data, completionHandler: {data, response, err in
                
                if let error = err {
                    print("error:", error)
                    
                    Util.showMessage(view: self, title: "Error", message: error.localizedDescription , alertTitle: "Ok")
                    
                    self.disableControl(flag: false)
                }
                
//                do {
                    guard let data = data else { return }
                    
                    let isValidToken = JSONParser.parse(function: Function.isValidToken, data: data) as! IS_VALID_TOKEN
                    
                    if (isValidToken.errorCode == "0") {
                        
                        self.disableControl(flag: false)
                        
                        // redirect to next page
                        print("succeed check token goto next page right now")
                        
                        DispatchQueue.main.async {
                            let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: Shared.PLAN_VIEW_CONTROLLER_NAME)
                        //    let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainTabBarController")
                            
                            self.show(vc as! UIViewController, sender: vc)
                        }
                    } else {
                        self.disableControl(flag: false)
                    }
                
                    
                    
//                } catch {
//                    print("error:", error)
//                    
//                   self.disableControl(flag: false)
//                   
//                }

                self.disableControl(flag: false)
            })
            
            
        } else {
            self.disableControl(flag: false)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // hides text fields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == "\n") {
            if ( textField == txtUsername ) {
                txtUsername.resignFirstResponder()
            }
            if ( textField == txtPassword ) {
                txtPassword.resignFirstResponder()
            }

            
            return false
        }
        return true
    }
    
    func validToke(token: String) -> Bool {
        
        
        
        return true
    }

    @IBAction func login(_ sender: Any) {
        
        /*
        let indicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        indicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        indicator.center = view.center
        imgSplash.addSubview(indicator)
        indicator.bringSubview(toFront: imgSplash)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        indicator.startAnimating()
        */
        
        if (txtUsername != nil && (txtUsername.text?.count)! > 0 &&
            txtPassword != nil && (txtPassword.text?.count)! > 0) {
            
            var request = URLRequest(url: URL(string: "https://magicporting.dtac.co.th:12011/seedbox/authen/")!)
            request.httpMethod = "POST"
            request.timeoutInterval = 10
            
            if ( Messaging.messaging().fcmToken != nil && (Messaging.messaging().fcmToken?.count)! > 0 ) {
                
                var fcm = Messaging.messaging().fcmToken
                if ( fcm == nil ) {
                    fcm = ""
                }
                
                request.httpBody = ("user=" + txtUsername.text! + "&pass=" + txtPassword.text! + "&fcm=" + fcm!).data(using: .utf8)
            } else {
                request.httpBody = ("user=" + txtUsername.text! + "&pass=" + txtPassword.text!).data(using: .utf8)
            }
            
            
            let session = URLSession.shared
            
            session.dataTask(with: request) {data, response, err in
                print("Entered the completionHandler")
                
                if let error = err {
                    print("error:", error)
                    
                    Util.showMessage(view: self, title: "Error", message: error.localizedDescription , alertTitle: "Ok")
//                    indicator.stopAnimating()
                    return
                }
                
//                do {
                    guard let data = data else { return }
                    
//                    guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
//                        
//                        else { return }
//                    print("json:", json)
//                    let errorCode = json["error_code"] as? String
//                    let errorMessage = json["error_message"] as? String
//                    let token = json["token"] as? String
                    let authen = JSONParser.parse(function: Function.authen, data: data) as! AUTHEN
                    
                    
                    if ( authen.errorCode == "0") {
                        
                        let defaults = UserDefaults.standard
                        defaults.set(authen.token, forKey: "token")
                        defaults.set(authen.user, forKey: "userlogin")
                        defaults.synchronize()
                        SeedUtil.token = authen.token
                        SeedUtil.userLogin = authen.user
                        
                        print(SeedUtil.token)
                        
//                        indicator.stopAnimating()
                        
                        // goto next page
                        // PlantViewController
                        DispatchQueue.main.async {
                        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: Shared.PLAN_VIEW_CONTROLLER_NAME)
                        //    let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainTabBarController")
                        self.show(vc as! UIViewController, sender: vc)
                        }
                        
                    } else {
                        

                        
                        Util.showMessage(view: self, title: NSLocalizedString("ERROR", comment: ""), message: NSLocalizedString("ERROR_DETAIL", comment: "") + "\n" + authen.errorMessage, alertTitle: NSLocalizedString("OK", comment: ""))
                        
                    }
                    
                    
//                    let authen = JSONParser.parse(function: Function.authen, data: data)
                    print(authen)
                    
                    print("end task1")
                    
                    
                    
                    
//                } catch {
//                    print("error:", error)
//                    
//                    
//                    Util.showMessage(view: self, title: "Error", message: error.localizedDescription , alertTitle: "Ok")
//                }
                print("end task2")
//                indicator.stopAnimating()
                
            }.resume()

            
        } else {
            
            Util.showMessage(view: self, title: NSLocalizedString("ERROR", comment: ""), message: NSLocalizedString("INVALID_USERNAME_PASSWORD", comment: ""), alertTitle: NSLocalizedString("OK", comment: ""))
            
        }
        
//        indicator.stopAnimating()
        
    }
    
    
   }

