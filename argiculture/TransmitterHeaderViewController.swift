//
//  TransmitterHeaderViewController.swift
//  argiculture
//
//  Created by thanachais on 22/4/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class TransmitterHeaderViewController: UIViewController {

    
    @IBOutlet weak var imgPlant: UIImageView!
    
    @IBOutlet weak var lblTransmitter: UILabel!
    
    
    @IBOutlet weak var butSetting: UIButton!
    
    @IBOutlet weak var imgBattery: UIImageView!
    
    @IBOutlet weak var imgWireless: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
    }
    
    func display(index: Int) {
        
        let tmpData = Shared.getData.data[index]
        
        lblTransmitter.text = NSLocalizedString("TRANSMITTERS", comment: "") + (tmpData.dispName.count > 0 ? tmpData.dispName : tmpData.sn )
        imgPlant.isHidden = true
        if (tmpData.plant != nil ) {
            lblTransmitter.text! += "\n" + (tmpData.plant?.type)!
            imgPlant.image = UIImage(named: Shared.getPlantImageName(tmpData.plant!.type))
            imgPlant.isHidden = false
        }
        lblTransmitter.numberOfLines = 0
        lblTransmitter.sizeToFit()
        lblTransmitter.setNeedsDisplay()
        
        
        //                headerCell.lblDateTime.text = tmpData.data.datetime
        butSetting.tag = index
        butSetting.addTarget(self, action: #selector(showSetting), for: .touchUpInside)
        butSetting.isHidden = true   // Hidden button setting, Use in future
        
        // Signal
        if ( tmpData.sgn == "1") {
            imgWireless.image = UIImage(named: "wireless_0")
        } else {
            imgWireless.image = UIImage(named: "wireless_4")
        }
        
        // Battery
        var batt = 0
        batt = Int( tmpData.batt )!
        if ( batt == 100  ) {
            imgBattery.image = UIImage(named: "bat_4")
        } else if (batt >= 60  ){
            imgBattery.image = UIImage(named: "bat_3")
        } else if (batt >= 30  ){
            imgBattery.image = UIImage(named: "bat_2")
        } else if (batt >= 5  ){
            imgBattery.image = UIImage(named: "bat_1")
        } else {
            imgBattery.image = UIImage(named: "bat_0")
        }
        
        
    }
    
    @objc func showSetting() {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
