//
//  ImageDetailViewController.swift
//  argiculture
//
//  Created by thanachais on 14/3/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class ImageDetailViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var butBack: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBAction func back(_ sender: Any) {
        dismissScreen()
        
    }
    
    let maxImages = ImageThumbnailCollectionViewController.images.count
    var imageIndex: NSInteger = ImageThumbnailCollectionViewController.imageIndex
    var tapGestureRecognizer: UITapGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped)) // put : at the end of method name
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped)) // put : at the end of method name
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(allTapped(tapGestureRecognizer:)))
        self.view.addGestureRecognizer(self.tapGestureRecognizer!)
        
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        
        
        lblInfo.isHidden = true
        lblInfo.text = NSLocalizedString("IMG_DETAIL_LBL_INFO_NOTFOUND", comment: "")
        
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.image
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        pageControl.numberOfPages = maxImages
        
        butBack.setTitle(NSLocalizedString("BACK", comment: ""), for: .normal)
        
        pageControl.currentPage = ImageThumbnailCollectionViewController.imageIndex + 1
        
        
//         self.loadImage(ImageThumbnailCollectionViewController.imageIndex)
//        let backgroundQueue = DispatchQueue(label: "com.app.queue", qos: .background)
//        backgroundQueue.async {
//            print("Run on background thread")
//
//        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadImage(ImageThumbnailCollectionViewController.imageIndex)
        
//        self.dismiss(animated: true, completion: {})
    }
    
    @objc  func swiped(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
                
            case UISwipeGestureRecognizer.Direction.right :
                print("User swiped right")
                
                // decrease index first
                
                if ( imageIndex == maxImages - 1 ) {
                    
                } else {
                    imageIndex = imageIndex - 1
                    
                    if ( imageIndex < 0 ) {
                        imageIndex = 0
                    }
                    
                    loadImage(imageIndex)
                }
                    
                
                    
                    
                    
              
                
                pageControl.currentPage = imageIndex
                
                // check if index is in range
                
                
                
                
                
                
            case UISwipeGestureRecognizer.Direction.left:
                print("User swiped Left")
                
                // increase index first
                if ( imageIndex + 1 == maxImages ) {
                    
                } else {
                    
                    imageIndex = imageIndex + 1
                    
                    if ( imageIndex >= maxImages ) {
                        imageIndex = maxImages - 1
                    }
                    
                    loadImage(imageIndex)
                    
                    pageControl.currentPage = imageIndex
                }
                
                
                
                
                
                
                
            default:
                break //stops the code/codes nothing.
                
                
            }
            
        }
        
        
    }
    
    func loadImage(_ imageIndex:Int) {
        
//        let url = URL(string: "https://i.pinimg.com/originals/7a/60/bc/7a60bc0ce22863afefeff3fdc463fce7.jp")
        let url = URL(string: ImageThumbnailCollectionViewController.images[imageIndex])
        
        let alert = Util.displayPendingAlert(uiViewController: self, message: "LOADING")
        HTTPUtil.getData(from: url! ) { data, response, error in
            
            
            
            guard let data = data, error == nil else {
                print("Download Finished Error")
                alert.dismiss(animated: true, completion: nil)
                return
            }
            DispatchQueue.main.async {
                alert.dismiss(animated: true, completion: nil)
            }
            print(response?.suggestedFilename ?? url!.lastPathComponent)
            print("Download Finished")
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if ( httpResponse.statusCode >= 200 && httpResponse.statusCode <= 200 ) {
                    
                    self.lblInfo.isHidden = true
                    
                    DispatchQueue.main.async {
                        self.image.image = UIImage(data: data)
                        
                    }
                   
                } else {
                    DispatchQueue.main.async {
                        self.image.image = UIImage(named: "img_notfound")
                        
                        self.lblInfo.isHidden = false
                    }
                }
                
            }
            
            
            
            
            
        }
    }
    
    @objc func allTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        //        let tappedImage = tapGestureRecognizer.view as! UIImageView
        print("imageTapped")
        
        
        dismissScreen()
    }
    
    func dismissScreen() {
        
        ImageThumbnailCollectionViewController.images.removeAll()
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
