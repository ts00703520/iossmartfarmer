//
//  Shared.swift
//  argiculture
//
//  Created by thanachais on 14/3/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import Foundation
import UIKit

class Shared {
    static let version: Double = 1.3
    static let appleStoreLink = "https://itunes.apple.com/th/app/smart-farmer/id1240069272?l=th&mt=8"
    
    static var getData: GET_DATA!
    static var listPlant: LIST_PLANTS!
    static var plantSetting: PLANT_SETTING!
    
    static let PLAN_VIEW_CONTROLLER_NAME = "MainTabBarController"
    static let HOUSE_HEIGHT = CGFloat(32)
    static let HOUSE_ARROW_HEIGHT = CGFloat(24)
    static let HOUSE_ARROW_HEIGHT_HALF = CGFloat(12)
    static var HOUSE_ROW_NO = 0
    
    static var plantArray: NSArray?
    
    static let imgOn = UIImage(named: "switch_on")
    static let imgOff = UIImage(named: "switch_off")
    
    static func getPlantImageName(_ type: String) -> String {
        
        var imageName = ""
        switch (type) {
        case "ทั่วไป":
            imageName = "plant_generic"
            break
        case "ข้าว":
            imageName = "plant_rice"
            break
        case "เมล่อน":
            imageName = "plant_melon"
            break
        case "มะเขือเทศ":
            imageName = "plant_tomato"
            break
        case "กุหลาบ":
            imageName = "plant_rose"
            break
        case "กล้วยไม้":
            imageName = "plant_orchid"
            break
        case "กำหนดเอง1", "custom1":
            imageName = "plant_user_define1"
            break
        case "กำหนดเอง2", "custom2":
            imageName = "plant_user_define2"
            break
        case "กำหนดเอง3", "custom3":
            imageName = "plant_user_define3"
            break
        case "เห็ดหลินจือ":
            imageName = "plant_lingzhi"
            break
        default:
            imageName = ""
        }
        
        return imageName
        
    }
    
}
