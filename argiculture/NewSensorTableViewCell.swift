//
//  NewSensorTableViewCell.swift
//  argiculture
//
//  Created by Thanachai on 3/28/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import Foundation
import UIKit

class NewSensorTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblSensor: UILabel!
    
    @IBOutlet weak var lblSensorNo: UILabel!
    
    @IBOutlet weak var imgFace: UIImageView!
    
    
    @IBOutlet weak var imgCircle1: UIImageView!
    
    @IBOutlet weak var imgType1: UIImageView!
    
    @IBOutlet weak var lblSensor1: UILabel!
    
    @IBOutlet weak var lblValue1: UILabel!
    
    @IBOutlet weak var lblDateTime1: UILabel!
    
    @IBOutlet weak var lblMin1: UILabel!
    
    @IBOutlet weak var lblMax1: UILabel!
    
    
    
    @IBOutlet weak var imgCircle2: UIImageView!
    
    @IBOutlet weak var imgType2: UIImageView!
    
    @IBOutlet weak var lblSensor2: UILabel!
    
    @IBOutlet weak var lblValue2: UILabel!
    
    @IBOutlet weak var lblDateTime2: UILabel!
    
    @IBOutlet weak var lblMin2: UILabel!
    
    @IBOutlet weak var lblMax2: UILabel!
    
    
    
    @IBOutlet weak var lblBattery: UILabel!
    
    @IBOutlet weak var lblWireless: UILabel!
    @IBOutlet weak var imgBattery: UIImageView!
    
    @IBOutlet weak var imgWireless: UIImageView!
    
    
}
