//
//  AppDelegate.swift
//  argiculture
//
//  Created by Thanachai on 2/27/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseMessaging
import Fabric
import Crashlytics


@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("\n\n\nfcm Firebase registration token: \(fcmToken)\n\n\n")
        SeedUtil.tokenFcm = fcmToken
        
        
        SeedUtil.refreshFcm(token: SeedUtil.token, fcm: SeedUtil.tokenFcm, completionHandler: {_,_ in })
        
        
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("\n\n\nfcm Received data message: \(remoteMessage.appData)\n\n\n")
    }
    // [END ios_10_data_message]
}

//extension AppDelegate : FIRMessagingDelegate {
//    // Receive data message on iOS 10 devices.
//    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
//        print("%@", remoteMessage.appData)
//    }
//}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var timer: Timer? = nil
    var GRANT_NOTIFICATION: Bool? = false
    let gcmMessageIDKey = "gcm.message_id"
    
//    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if (launchOptions != nil)
        {
            //do some thing special e.g. display particular ViewController or Set some notification badge value.
            
            
        }
        else {
            
            FirebaseApp.configure()
            
            // [START set_messaging_delegate]
            Messaging.messaging().delegate = self
            // [END set_messaging_delegate]
            // Register for remote notifications. This shows a permission dialog on first run, to
            // show the dialog at a more appropriate time move this registration accordingly.
            // [START register_for_notifications]
            if #available(iOS 10.0, *) {
                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.current().delegate = self
                
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(
                    options: authOptions,
                    completionHandler: {_, _ in })
            } else {
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                application.registerUserNotificationSettings(settings)
            }
            
            application.registerForRemoteNotifications()
            
            // [END register_for_notifications]
            
            
            
            
            // Override point for customization after application launch.
            let defaults = UserDefaults.standard
            let token = defaults.string(forKey: "token")
            let userLogin = defaults.string(forKey: "userlogin")
            let refreshInterval = defaults.string(forKey: "refreshInterval")
            //        let enableNotification = defaults.string(forKey: "enableNotification")
            //        var startTime = defaults.string(forKey: "startTime")
            //        var stopTime = defaults.string(forKey: "stopTime")
            
            //        startTime = Util.str(startTime)
            //        stopTime = Util.str(stopTime)
            //        if (startTime == "") {
            //            startTime = SeedUtil.defaultStartTime
            //        }
            //
            //        if (stopTime == "") {
            //            stopTime = SeedUtil.defaultStopTime
            //        }
            
            
            SeedUtil.token = Util.str(token)
            SeedUtil.userLogin = Util.str(userLogin)
            //        SeedUtil.enableNotification = Util.str(enableNotification)
            if (  Util.str(refreshInterval).count == 0  ) {
                SeedUtil.refreshInterval = SeedUtil.defaultRefreshInterval
            } else {
                SeedUtil.refreshInterval = Util.str(refreshInterval)
            }
        }
        

        Fabric.with([Crashlytics.self])
        
        return true
    }
    func willResignActive(_ notification: NSNotification) {
        /*
        print("willResignActive call ")
        
        if (timer != nil ) {
            timer?.invalidate()
        }
        
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.processNotification ), userInfo: nil, repeats: true)
        print("refresh : \(SeedUtil.refreshInterval) current datetime: \(Util.getDateFormat(format: "dd.MM.yyyy HH:mm:ss:SSS"))")
        
        print("start timer task \nbecause enable notification")
        */
        
//        processNotification()
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        print("applicationWillResignActive")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        print("application will enter background")
        NewPlantViewControllerV2.stopTimer()
//
//        if ( SeedUtil.enableNotification == "0") {
//            print("stop get plant, get data timer \nbecause disable notification")
//            
//            
//        } else {
//            
//            if (timer != nil ) {
//                timer?.invalidate()
//            }
//            
//            timer = Timer.scheduledTimer(timeInterval: Double(SeedUtil.refreshInterval)! * 60, target: self, selector: #selector(self.processNotification ), userInfo: nil, repeats: true)
//            print("refresh : \(SeedUtil.refreshInterval) current datetime: \(Util.getDateFormat(format: "dd.MM.yyyy HH:mm:ss:SSS"))")
//
//            print("start timer task \nbecause enable notification")
//            
////            let delay = Int(1 * Double(1000))
////            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // in half a second...
////                self.processNotification()
////            }
//        
//        }

        
        
    }
    
    

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        print("application will enter foreground")
        if ( timer != nil ) {
            print("stop background timer fetch")
            timer?.invalidate()
        }
        
        checkVersion()
        
        
    }
    
    func checkVersion() {
        
        SeedUtil.checkVersion(os: "ios", completionHandler: {result, message, object in
            
            if result == 0 {
                
                let ret = object as! VERSION
                
                if ( Shared.version < Double( ret.version)! ) {
                    
                    let alert = UIAlertController(title: NSLocalizedString("VERSION", comment: ""), message: NSLocalizedString("UPDATE_VERSION", comment: ""), preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { action in
                        
                        DispatchQueue.main.async {
                            UIApplication.shared.openURL(URL(string: Shared.appleStoreLink)!)
                        }
                        
                    }))
                    DispatchQueue.main.async {
                        
                        self.window?.rootViewController!.present(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                
            } else {
                print("error message \(message)")
            }
            
            
        })
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        
        SeedUtil.checkToken(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken, completionHandler: { result, message in
            if (result == 0) {
                print("applicationDidBecomeActive success")
                
                //                self.refreshPlant() { () -> () in
                //                    self.refreshGetData()
                //                }
                
                
            } else if (result > 1 || result < 0) {
                DispatchQueue.main.async {
                    
//                    let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
//                    self.show(vc as! UIViewController, sender: vc)
                    
                    
                }
            }
        })

        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        NewPlantViewControllerV2.stopTimer()
        if ( timer != nil ) {
            print("stop background timer fetch")
            timer?.invalidate()
        }

    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("perform fetch with completion handler")
        
        
        // stop PlantViewTimer
        NewPlantViewControllerV2.stopTimer()
        
//        if ( SeedUtil.enableNotification == "0") {
//            print("stop get plant, get data timer \nbecause disable notification")
//            
//            
//        } else {
//            
//            if (timer != nil ) {
//                timer?.invalidate()
//            }
//            
//            
//            timer = Timer.scheduledTimer(timeInterval: Double(SeedUtil.refreshInterval)! * 60, target: self, selector: #selector(self.processNotification ), userInfo: nil, repeats: true)
//            print("refresh : \(SeedUtil.refreshInterval) current datetime: \(Util.getDateFormat(format: "dd.MM.yyyy HH:mm:ss:SSS"))")
//            
//        }
        
        
//        timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.printtest), userInfo: nil, repeats: true)
        

        completionHandler(.newData)
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
//         Print full message.
        print(userInfo)
//
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("=========================")
        print("\n\n")
        print("Unable to register for remote notifications: \(error.localizedDescription)")
        print("=========================")
        print("\n\n")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("=========================")
        print("\n\n")
        print("APNs token retrieved: \(deviceToken)")
        Messaging.messaging().apnsToken = deviceToken
        
        print("=========================")
        print("\n\n")
    }
    
    func printtest() {
        print("current datetime: \(Util.getDateFormat(format: "dd.MM.yyyy HH:mm:ss:SSS"))")
        print("timer backgroud task\n")
    }
    
        
    
    
    /*
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }
    */

}

