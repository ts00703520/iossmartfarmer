//
//  SensorBodyTableViewCell.swift
//  argiculture
//
//  Created by thanachais on 6/3/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import Foundation
import UIKit

class SensorBodyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgBackground: UIImageView!
    
    @IBOutlet weak var imgSensorIndicator: UIImageView!
    
    @IBOutlet weak var imgSensor: UIImageView!
    
    @IBOutlet weak var lblDataType: UILabel!
    
    @IBOutlet weak var lblDataValue: UILabel!
    
    @IBOutlet weak var txtDataValue: UITextField!
    @IBOutlet weak var lblDataUnit: UILabel!
   
    @IBOutlet weak var imgMax: UIImageView!
    
    @IBOutlet weak var imgMin: UIImageView!
    
    @IBOutlet weak var lblIndicator: UILabel!
    
    
    @IBOutlet weak var lblMin: UILabel!
    
    @IBOutlet weak var lblMax: UILabel!
    
}


