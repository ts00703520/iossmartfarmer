//
//  SetMinMaxViewController.swift
//  argiculture
//
//  Created by thanachais on 30/4/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class SetMinMaxViewController: UIViewController {

    var index:Int!
    var savePlantSetting: RANGE_MODIFY!
    
    var currentImage: String!
    let sensorList =  ["temperature", "humidity", "light", "soil_humidity", "co2"]
    
    @IBOutlet weak var lblPlantSetting: UILabel!
    @IBOutlet weak var tblPlant: UITableView!
    @IBOutlet weak var butRight: UIButton!
    @IBOutlet weak var butLeft: UIButton!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblPlant.dataSource = self
        tblPlant.delegate = self
        
        self.tblPlant.contentInset.bottom = 10
        self.tblPlant.separatorColor = UIColor.clear
        self.tblPlant.allowsSelection = false
    
        
        // Do any additional setup after loading the view.
      
        
        pageControl.numberOfPages = 1
        pageControl.currentPage = 0
        
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.right {
            print("Swipe Right")
            
            if ( self.pageControl.currentPage > 0 ) {
                self.pageControl.currentPage = self.pageControl.currentPage - 1
                
                
//                DispatchQueue.main.async {
//
//                    self.displayValueMinMax(sensor: self.sensorList[self.pageControl.currentPage])
//                    self.displayImageSensor()
//                }
                
            }
            
            
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.left {
            print("Swipe Left")
            
            if ( self.pageControl.currentPage < sensorList.count ) {
                self.pageControl.currentPage = self.pageControl.currentPage + 1
                
//                DispatchQueue.main.async {
//
//                    self.displayValueMinMax(sensor: self.sensorList[self.pageControl.currentPage])
//                    self.displayImageSensor()
//                }
                
            }
            
        }
        
        index = self.pageControl.currentPage
        
        DispatchQueue.main.async {
            
            self.refreshScreen()
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SeedUtil.plantSetting(token: SeedUtil.token, plant: "", new_name: "", temp_min: "", temp_max: "", hu_min: "", hu_max: "", light_min: "", light_max: "", soil_hu_min: "", soil_hu_max: "", co2_min: "", co2_max: "") { result, message, obj in
            
            if ( result == 0) {
                
                Shared.plantSetting = (obj as! PLANT_SETTING)
                
                if ( Shared.plantSetting!.list!.count == 0 ) {
                    
                    
                    
                } else {
                    
                    
                    DispatchQueue.main.async {
                        self.pageControl.numberOfPages = Shared.plantSetting.list?.count ?? 0
                        
                        
                    }
                }
                
                DispatchQueue.main.async {
                    self.refreshScreen()
                }
                
                
            } else {
                
            }
        }
        
    }
    
    @IBAction func rightClick(_ sender: Any) {
        if ( self.pageControl.currentPage < sensorList.count ) {
            
            self.pageControl.currentPage = self.pageControl.currentPage + 1
      
        }
        
        index = self.pageControl.currentPage
        
        DispatchQueue.main.async {
            
            self.refreshScreen()
        }
        
    }
    
    @IBAction func leftClick(_ sender: Any) {
        
        if ( self.pageControl.currentPage > 0 ) {
            
            self.pageControl.currentPage = self.pageControl.currentPage - 1
            
        }
        
        index = self.pageControl.currentPage
        
        DispatchQueue.main.async {
            
            self.refreshScreen()
        }
    }
    
    func refreshScreen() {
        
        if ( Shared.plantSetting != nil ) {
            
            if Shared.plantSetting.list!.count == 1 {
                
                self.butLeft.isHidden = true
                self.butRight.isHidden = true
                
            } else {
                
                self.butLeft.isHidden = false
                self.butRight.isHidden = false
                
            }
            
            self.lblPlantSetting.text = Shared.plantSetting.list![pageControl.currentPage].type
        }
        
        self.tblPlant.reloadData()
        
        
    }
    
    
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
}

extension SetMinMaxViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Shared.plantSetting == nil {
            return 0
        }
        
        let range = Shared.plantSetting.list![pageControl.currentPage]
        savePlantSetting = RANGE_MODIFY.init(type: range.type, temp_min: range.temp_min, temp_max: range.temp_max,
                                             hu_min: range.hu_min, hu_max: range.hu_max, light_min: range.light_min,
                                             light_max: range.light_max, soi_hu_min: range.soi_hu_min, soi_hu_max: range.soi_hu_max, co2_min: range.co2_min, co2_max: range.co2_max)
        
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var strLblMax = "SET_MIN_MAX_LBL_MAX", strLblMin = "SET_MIN_MAX_LBL_MIN"
        var imageName = ""
        
        let range = Shared.plantSetting.list![pageControl.currentPage]
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "setMinMaxCell", for: indexPath) as! SetMinMaxTableViewCell
        
        
        
        cell.viewBody.layer.cornerRadius = 10
        
        cell.txtMax.delegate = self
        cell.txtMin.delegate = self
        cell.txtMax.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        cell.txtMin.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        cell.txtMax.layer.cornerRadius = 20
        cell.txtMin.layer.cornerRadius = 20
        
        let tag = 2 * indexPath.row
        
        cell.txtMax.tag = tag
        cell.txtMin.tag = tag + 1
        
        if (indexPath.row == 0) {
            
            imageName = "temperature"
            
            cell.lblMin.text = ""
            
            cell.txtMax.text = range.temp_max
            cell.txtMin.text = range.temp_min
            
            strLblMax = "SET_MIN_MAX_LBL_TEMP_MAX"
            strLblMin = "SET_MIN_MAX_LBL_TEMP_MIN"
            
        } else if (indexPath.row == 1) {
            
            imageName = "light"
            
            cell.txtMax.text = range.light_max
            cell.txtMin.text = range.light_min
            
            strLblMax = "SET_MIN_MAX_LBL_LIGHT_MAX"
            strLblMin = "SET_MIN_MAX_LBL_LIGHT_MIN"
            
           
            
        } else if (indexPath.row == 2) {
            
            imageName = "humidity"
            
            cell.txtMax.text = range.hu_max
            cell.txtMin.text = range.hu_min
            
            strLblMax = "SET_MIN_MAX_LBL_HU_MAX"
            strLblMin = "SET_MIN_MAX_LBL_HU_MIN"
            
            
        } else if (indexPath.row == 3) {
            
            imageName = "soil_humidity"
            
            cell.txtMax.text = range.soi_hu_max
            cell.txtMin.text = range.soi_hu_min
            
            strLblMax = "SET_MIN_MAX_LBL_SOIL_HU_MAX"
            strLblMin = "SET_MIN_MAX_LBL_SOIL_HU_MIN"
            
        } else if (indexPath.row == 4) {
            
            imageName = "co2"
            
            cell.txtMax.text = range.co2_max
            cell.txtMin.text = range.co2_min
            
            strLblMax = "SET_MIN_MAX_LBL_CO2_MAX"
            strLblMin = "SET_MIN_MAX_LBL_CO2_MIN"
            
        }
        
        
        
        cell.imgPlant.layer.borderColor = UIColor.dtacBrownGrey.cgColor
        cell.imgPlant.layer.borderWidth = 1
        cell.imgPlant.layer.masksToBounds = false
        cell.imgPlant.layer.cornerRadius = cell.imgPlant.frame.height/2
        cell.imgPlant.clipsToBounds = true
        cell.imgPlant.image = UIImage(named: imageName)
        
        cell.lblMin.text = NSLocalizedString(strLblMin, comment: "")
        cell.lblMax.text = NSLocalizedString(strLblMax, comment: "")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    
}


extension SetMinMaxViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        
        let result = string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
        
        if ( (textField.text?.count)! + string.count > 5) {
//             textField.text = "23"
            //            SeedUtil.refreshInterval = "99"
            return false
        }
//        else if ( ((textField.text?.count)! + string.count == 2 ) && Int(textField.text!)! > 23) {
//            textField.text = "23"
//            //            SeedUtil.refreshInterval = "99"
//            return false
//        }
        
//        if result {
//            
//            let allText = textField.text!
//            
//            if textField.tag == 0 {
//                self.savePlantSetting.temp_max = allText
//            } else if textField.tag == 1 {
//                self.savePlantSetting.temp_min = allText
//            } else if textField.tag == 2 {
//                self.savePlantSetting.light_max = allText
//            } else if textField.tag == 3 {
//                self.savePlantSetting.light_min = allText
//            } else if textField.tag == 4 {
//                self.savePlantSetting.hu_max = allText
//            } else if textField.tag == 5 {
//                self.savePlantSetting.hu_min = allText
//            } else if textField.tag == 6 {
//                self.savePlantSetting.soi_hu_max = allText
//            } else if textField.tag == 7 {
//                self.savePlantSetting.soi_hu_min = allText
//            } else if textField.tag == 8 {
//                self.savePlantSetting.co2_max = allText
//            } else if textField.tag == 9 {
//                self.savePlantSetting.co2_min = allText
//            }
//        }
        
        return result
    }
    
    
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        let allText = textField.text!
        
        if textField.tag == 0 {
            self.savePlantSetting.temp_max = allText
        } else if textField.tag == 1 {
            self.savePlantSetting.temp_min = allText
        } else if textField.tag == 2 {
            self.savePlantSetting.light_max = allText
        } else if textField.tag == 3 {
            self.savePlantSetting.light_min = allText
        } else if textField.tag == 4 {
            self.savePlantSetting.hu_max = allText
        } else if textField.tag == 5 {
            self.savePlantSetting.hu_min = allText
        } else if textField.tag == 6 {
            self.savePlantSetting.soi_hu_max = allText
        } else if textField.tag == 7 {
            self.savePlantSetting.soi_hu_min = allText
        } else if textField.tag == 8 {
            self.savePlantSetting.co2_max = allText
        } else if textField.tag == 9 {
            self.savePlantSetting.co2_min = allText
        }
    }
        
    
    
}
