//
//  HTTPUtil.swift
//  argiculture
//
//  Created by Thanachai on 3/8/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import Foundation
import UIKit

class HTTPUtil {
    
    enum METHOD {
        case GET, POST
    }
    
    static func async(url: String, method: METHOD, secTimeout: Double, data: String, completionHandler:  @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        var request = URLRequest(url: URL(string: url)!)
        var httpMethod = "POST"
        switch (method) {
        case .GET:
             httpMethod = "GET"
             break
        case .POST:
                httpMethod = "POST"
            break
        }
        
        request.httpMethod = httpMethod
        request.timeoutInterval = secTimeout
        request.httpBody = (data).data(using: .utf8)
        let session = URLSession.shared
        
        print("HTTPUtil async post data: \(data)")
        
        session.dataTask(with: request) {data, response, err in
            print("HTTPUtil async")
            
            completionHandler(data, response, err)
            
        }.resume()

    }
    
    static func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}
