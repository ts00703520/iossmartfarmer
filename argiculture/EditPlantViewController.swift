//
//  EditPlantViewController.swift
//  argiculture
//
//  Created by thanachais on 18/4/2562 BE.
//  Copyright © 2562 DTAC. All rights reserved.
//

import UIKit

class EditPlantViewController: UIViewController {

    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var lblSubMenu: UILabel!
    @IBOutlet weak var butBack: UIButton!
    
    @IBOutlet weak var tblView: UITableView!
    
    var selectedIndexPath: IndexPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblMenu.text = NSLocalizedString("EDIT_PLANT_MENU", comment: "")
        lblSubMenu.text = NSLocalizedString("EDIT_PLANT_SUB_MENU", comment: "")
        butBack.setTitle(NSLocalizedString("EDIT_PLANT_BUT_BACK", comment: ""), for: .normal)  
        
        self.tblView.dataSource = self
        self.tblView.delegate = self
        self.tblView.separatorColor = UIColor.clear
        self.tblView.contentInset.bottom = 10
        
    }
    
    
    

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    

}

extension EditPlantViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Shared.getData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "editPlantCell", for: indexPath) as! EditPlantTableViewCell
        
        cell.selectionStyle = .none
        
        cell.bgView.layer.cornerRadius = 5
        cell.bgView.layer.masksToBounds = true
        
        cell.lblPlant.numberOfLines = 0
        cell.lblPlant.text = NSLocalizedString("TRANSMITTERS", comment: "") + " " +   (Shared.getData.data[indexPath.row].dispName.count > 0 ? Shared.getData.data[indexPath.row].dispName : Shared.getData.data[indexPath.row].sn ) + "\n"
        
        
        
        if Shared.getData.data[indexPath.row].plant != nil && Shared.getPlantImageName(Shared.getData.data[indexPath.row].plant!.type).count > 0 {
            
            cell.imgPlant.image = UIImage(named: Shared.getPlantImageName(Shared.getData.data[indexPath.row].plant!.type))
            cell.lblPlant.text = cell.lblPlant.text! + Shared.getData.data[indexPath.row].plant!.type
        }
        
        
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        self.selectedIndexPath = indexPath
//        self.performSegue(withIdentifier: "SelectPlantView", sender: self)
//        
//        
//    }
    
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        self.selectedIndexPath = indexPath
        self.performSegue(withIdentifier: "SelectPlantView", sender: self)
        
        
        
        return indexPath
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if (segue.identifier == "SelectPlantView") {
            let viewController = segue.destination as! SelectPlantViewController
            viewController.index = selectedIndexPath.row
           
           
            
        }
        
    }
    
    
}
