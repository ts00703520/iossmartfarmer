//
//  MainTemplateViewController.swift
//  argiculture
//
//  Created by Thanachai on 6/1/2560 BE.
//  Copyright © 2560 DTAC. All rights reserved.
//

import UIKit
import Firebase

class MainTemplateViewController: UIViewController {
    
    @IBOutlet weak var butMenu: UIButton!
    @IBOutlet weak var butUser: UIButton!
    @IBOutlet weak var lblUser: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
       
//        lblFullname.text = NSLocalizedString("USERNAME", comment: "") + SeedUtil.userLogin
//        lblUserId.text = NSLocalizedString("USERNAME", comment: "") + SeedUtil.userLogin
        
        SeedUtil.checkToken(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken, completionHandler: { result, message in
            if (result == 0) {
                print("view did appear check token success")
                
//                self.refreshPlant() { () -> () in
//                    self.refreshGetData()
//                }
                
                
            } else if (result > 1 || result < 0) {
                DispatchQueue.main.async {
                    
                    let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                    self.show(vc as! UIViewController, sender: vc)
                }
            }
        })
        
        
        
        
        
    }
    
    
    @IBAction func logout(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("LOGOUT_TITLE", comment: ""), message: NSLocalizedString("LOGOUT_MESSAGE", comment: ""), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: UIAlertAction.Style.default, handler: { action in
            
            SeedUtil.logout(token: SeedUtil.token, fcm: Messaging.messaging().fcmToken,   completionHandler: {
                
                result, message in
                
                if ( result == 0 ) {
                    
                    NewPlantViewController.stopTimer()
                    
                    DispatchQueue.main.async {
                        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                        self.show(vc as! UIViewController, sender: vc)
                    }
                    
                } else {
                    
                    
                    DispatchQueue.main.async {
                        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
                        self.show(vc as! UIViewController, sender: vc)
                    }
                    
                }
                
                
            })
            
            
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func menuClicked(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
